"""
A user module for Flask.

"""

from schlange.data.login import login_manager

from schlange.user.activation import blueprint as activation_blueprint
from schlange.user.login import blueprint as login_blueprint
from schlange.user.recovery import blueprint as recovery_blueprint
from schlange.user.register import blueprint as register_blueprint

from schlange.user.models.user import User

def init(app):
    """Initalise the shop by registering all the blueprint with the given app.

    Args:
        app: The flask application to register the blueprints with.
    """
    app.register_blueprint(activation_blueprint, url_prefix='/user/activation')
    app.register_blueprint(login_blueprint, url_prefix='/user/log')
    app.register_blueprint(recovery_blueprint, url_prefix='/user/recovery')
    app.register_blueprint(register_blueprint, url_prefix='/user/register')

    # TODO: Put this in a function as in http://bit.ly/1qYp9b6
    login_manager.user_loader(lambda id: User.query.get(id))