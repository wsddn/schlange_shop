from schlange import create_app
import unittest

class SchlangeTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app('../tests/config.py')

    def tearDown(self):
        pass