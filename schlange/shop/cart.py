"""
The cart module of the shop implements a Flask Blueprint based
front-end for manipulation of the shopping cart. Current shopping
cart implementation is hard coded as a Redis based one.
"""

# TODO: Implement a RESTful API for the cart.
# TODO: Don't hard-code the Redis cart.

from decimal import Decimal
from flask import (Blueprint, jsonify, render_template, request, redirect,
                   url_for)
from schlange.lib.shopstorage import CartStorage
from schlange.lib.localisation import Localisation
from schlange.shop.models import ProductModel
from schlange.shop.models.currency import format_currency, round_currency

blueprint = Blueprint('cart', __name__, template_folder='templates',
                      static_folder='static')


def get_raw_cart():
    """Return the cart as a list of tuples (ProductModel, quantity)"""
    cart_data = CartStorage().get_all()

    product_models = ProductModel.query.filter(
        ProductModel.id.in_(list(cart_data.keys()))
    ).all()

    return [(m, cart_data[m.id]) for m in product_models]


def get_cart():
    """Return a python dict representation of the cart.
    Format:
        {
            'products': {
                '3 (model id)': {
                    'id': 3,
                    'name': 'Fast Toaster',
                    'img_src': 'http://example.com/toaster.png',
                    'quantity': 10,
                    'price': '$20.33',
                    'subtotal': '$203.30'
                }
            },
            'total': '$203.30'
            'total_raw': 203.30:
        }
    """

    # TODO: Add mutable flag to each product.

    cart_data = get_raw_cart()
    local = Localisation()
    cart_struct = {'products': {}, 'total': '', 'total_raw': Decimal(0)}

    for (model, quantity) in cart_data:
        subtotal = round_currency(
            model.net_price(local.currency_id, local.country_id) * quantity,
            local.currency_id
        )

        cart_struct['total_raw'] += subtotal
        cart_struct['products'][model.id] = {
            'id': model.id,
            'name': model.name,
            'img_src': '',
            'quantity': quantity,
            'price': model.net_format_price(local.currency_id,
                                            local.country_id),
            'subtotal': format_currency(subtotal, local.currency_id)
        }

    cart_struct['total'] = format_currency(cart_struct['total_raw'],
                                           local.currency_id)
    return cart_struct


@blueprint.route('/')
def index():
    """Return a rendered AJAX enabled cart overview."""
    cart = get_cart()
    return render_template('cart/index.html', cart_data=cart)


@blueprint.route('/get')
def get():
    """Return a JSON encoded representation of the cart. See get_cart_data()
    for the layout"""
    data = get_cart()
    return jsonify(data)


@blueprint.route('/add')
def add():
    """Add a product to the cart with the given HTTP GET product and
    quantity fields."""
    cart = CartStorage()
    cart.add(request.args['product'], request.args['quantity'])

    return redirect(url_for('cart.index'))


@blueprint.route('/delete')
def delete():
    """Remove the product (id given by HTTP GET) from the cart"""
    cart = CartStorage()
    cart.delete(request.args['product'])

    return redirect(url_for('cart.index'))


@blueprint.route('/update', methods=['POST'])
def update():
    """Update the cart with the HTTP POST product and quantity fields"""
    cart = CartStorage()
    cart.update(request.form['product'], request.form['quantity'])

    return redirect(url_for('cart.index'))


#@blueprint.route('/all', methods=('DELETE'))
#def delete_all():
#    cart = CartStorage()
#    cart.remove_all()

#    return redirect(url_for('cart.index'))
