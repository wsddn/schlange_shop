from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from smtplib import SMTP

def send_normal_email(from_addr, to_addr, header=[], html=None, text=None):
    msg = MIMEMultipart('alternative')
    msg['To'] = to_addr
    msg['From'] = from_addr

    for key, value in header.items():
        msg[key.title()] = value

    if text is not None:
        msg.attach(MIMEText(text, 'plain'))

    if html is not None:
        msg.attach(MIMEText(html, 'html'))

    smtp = SMTP(app.config.get('EMAIL_SMTP_HOST', 'localhost'))
    smtp.sendmail(from_addr, to_addr, msg.as_string())
    smtp.quit()
