from datetime import datetime
from flask import (Blueprint, render_template, request, redirect, url_for)

from schlange.data.database import db
from schlange.user.activation import send_activation_email
from schlange.user.models.user import User, generate_salt, hash_password
from schlange.user.forms import RegisterForm

blueprint = Blueprint('register', __name__, template_folder='templates',
                      static_folder='static')

@blueprint.route('/', methods=('GET', 'POST'))
def register():
    form = RegisterForm()

    if form.validate_on_submit():
        # TODO: Remove when debugging 
        register_user(form.email.data, form.password.data)
        return redirect(url_for('register.done'))

    return render_template('register.html', form=form)

@blueprint.route('/done')
def done():
    return render_template('register/done.html')

def register_user(email, password, active=False):
    # TODO: Is it right to assume that the email will be valid and unique?
    # TODO: Give a proper way of returning many different error codes.
    # TODO: Look for a customer to link the account to.
    # TODO: Send an activation email.
    # TODO: Should we prefix the password, to be forwards compatible?
    # TODO: What should be returned. User object?

    salt = generate_salt()
    user = User(
        email=email,
        password=hash_password(salt, password),
        salt=salt,
        date_created=datetime.now(),
        active=active
    )

    db.session.add(user)
    db.session.commit()

    if not active:
        pass
        # TODO: implement
        send_activation_email(user)

    return user
