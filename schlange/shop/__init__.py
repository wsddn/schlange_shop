"""
A shop module for Flask. It is a collection of blueprints and utilties
which create an online shop, useful for selling items. Specifically
designed for eggxactly ltd.

"""
from schlange.shop.cart import blueprint as cart_blueprint
from schlange.shop.checkout import blueprint as checkout_blueprint
from schlange.shop.product import blueprint as product_blueprint


def init(app):
    """Initalise the shop by registering all the blueprint with the given app.

    Args:
        app: The flask application to register the blueprints with.
    """
    app.register_blueprint(product_blueprint, url_prefix='/shop/product')
    app.register_blueprint(cart_blueprint, url_prefix='/shop/cart')
    app.register_blueprint(checkout_blueprint, url_prefix='/shop/checkout')
