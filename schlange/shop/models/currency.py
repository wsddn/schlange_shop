from schlange.data.database import db
from decimal import Decimal, ROUND_HALF_UP

class Currency(db.Model):
    __tablename__ = 'currencies'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    symbol = db.Column(db.String(5), nullable=False)
    code = db.Column(db.String(5), nullable=False)
    decimal_places = db.Column(db.Integer, nullable=False)
    countries = db.relationship('Country', backref='currency', lazy='dynamic')

def round_currency(amount, currency_id):
    """Rounds the amount to the decimal places required by the
    given currency

    Args:
        amount: The amount of money to round, must be Decimal.
        currency_id: The currency to round the amount as.

    Returns:
        A Decimal with the currency rounded to the correct decimal places.
    """

    currency = Currency.query.get(currency_id)
    return amount.quantize(Decimal('10') ** -currency.decimal_places, 
                           rounding=ROUND_HALF_UP)

def format_currency(amount, currency_id):
    """Formats the amount with the given currency.

    Args:
        amount: The amount of money to format.
        currency_id: The currency to format the amount as

    Returns:
        A unicode string with the currency symbol and appropiate decimal
        places.
    """
    # TODO: Don't limit the decimal places to 2.
    # Maybe get rid of '2f' replace with 'n'.
    currency = Currency.query.get(currency_id)
    rounded_amount = round_currency(amount, currency_id)
    return '{}{:.2f}'.format(currency.symbol, rounded_amount)

class CurrencyConversion(db.Model):
    __tablename__ = 'currency_conversions'
    id = db.Column(db.Integer, primary_key=True)
    currency_from_id = db.Column(db.Integer, db.ForeignKey('currencies.id'))
    currency_to_id = db.Column(db.Integer, db.ForeignKey('currencies.id'))
    rate = db.Column(db.Numeric)

def convert_currency(amount, from_id, to_id):
    """Converts an amount of money in one currency to another.

    Converts an amount from one currency to another using the database.

    Args:
        amount: The amount of money to convert.
        from_id: The id of the currency the amount id currently in.
        to_id: The id of the currency to convert the amount into.

    Returns:
        If a conversion for the two currencies does not exist then None.
        Else a float with the amount converted to the desired currency.
    """

    if from_id == to_id:
        return amount

    # TODO: See if IN is more appropriate than these OR statements.
    # TODO: Use another SQL construct such as ANY
    conversion = CurrencyConversion.query.filter((
            (CurrencyConversion.currency_from_id == from_id) &
            (CurrencyConversion.currency_to_id == to_id)
        ) | (
            (CurrencyConversion.currency_from_id == to_id) &
            (CurrencyConversion.currency_to_id == from_id)
        )
    ).first()

    if conversion is None:
        return None

    # Found a conversion that we have to do in reverse.
    if conversion.currency_from_id == to_id:
        return amount / conversion.rate

    return amount * conversion.rate

class Discount(db.Model):
    __tablename__ = 'discounts'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    fixed = db.Column(db.Numeric)
    fixed_currency_id = db.Column(db.Integer, db.ForeignKey('currencies.id'))
    percent = db.Column(db.Numeric)

tax_country_maps = db.Table(
    'tax_country_maps',
    db.Column('tax_id', db.Integer, db.ForeignKey('taxes.id')),
    db.Column('country_id', db.Integer, db.ForeignKey('countries.id'))
)

class Tax(db.Model):
    __tablename__ = 'taxes'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    percentage = db.Column(db.Numeric)
    fixed = db.Column(db.Numeric)
    fixed_currency_id = db.Column(db.Integer, db.ForeignKey('currencies.id'))
    countries = db.relationship('Country',
        secondary=tax_country_maps
    )

def calculate_tax(amount, currency_id, taxes):
    """Calculate the amount of tax, in the given currency_id.

    Args:
        amount: The amount to calculate tax on.
        currency_id: The currency that the amount is in.        
        taxes: A list of Tax objects which are used to calculate
               the net amount.

    Returns:
        The tax as a Decimal.
    """

    total_tax = Decimal(0)
    for tax in taxes:
        if tax.percentage is not None:
            total_tax += amount * (tax.percentage/100)
        elif tax.fixed is not None:
            total_tax += convert_currency(tax.fixed, tax.fixed_currency_id,
                                    currency_id)

    return round_currency(total_tax, currency_id)


class PaymentProcessorCurrency(db.Model):
    __tablename__ = 'payment_processor_currencies'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(10), nullable=False)
    payment_processor_id = db.Column(db.Integer,
                                     db.ForeignKey('payment_processors.id'),
                                     nullable=False)
    currency_id = db.Column(db.Integer, db.ForeignKey('currencies.id'),
              nullable=False)
    currency = db.relationship('Currency')

class PaymentProcessor(db.Model):
    __tablename__ = 'payment_processors'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    display_name = db.Column(db.String(100), nullable=False)
    default_processor_currency_id = db.Column(db.Integer,
                                              db.ForeignKey('currencies.id'))
    # TODO: Is the dynamic required.
    processor_currencies = db.relationship('PaymentProcessorCurrency',
                                           lazy='dynamic',
                                           backref='payment_processor')
    default_processor_currency = db.relationship('PaymentProcessorCurrency')

