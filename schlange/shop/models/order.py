from schlange.data.database import db

# TODO: Document this.
class Order(db.Model):
    __tablename__ = 'orders'
    id = db.Column(db.Integer, primary_key=True)
    billing_customer_id = db.Column(db.Integer, db.ForeignKey('customers.id'),
                                   nullable=False)
    billing_address_id = db.Column(db.Integer, db.ForeignKey('addresses.id'),
                                   nullable=False)
    delivery_first_name = db.Column(db.String(100), nullable=False)
    delivery_last_name = db.Column(db.String(100), nullable=False)
    delivery_address_id = db.Column(db.Integer, db.ForeignKey('addresses.id'),
                                    nullable=False)
    price = db.Column(db.Numeric, nullable=False)
    price_currency_id = db.Column(db.Integer, db.ForeignKey('currencies.id'),
                                  nullable=False)
    completed = db.Column(db.Boolean, nullable=False, default=False)
    failed = db.Column(db.Boolean, nullable=False, default=False)
    payment_processor_id = db.Column(db.Integer,
                                   db.ForeignKey('payment_processors.id'),
                                   nullable=False)
    payment_processor_currency_id = db.Column(
        db.Integer,
        db.ForeignKey('payment_processor_currencies.id'),
    )
    payment_detail = db.Column(db.String(1024))
    shipping_method_id = db.Column(db.Integer,
                                   db.ForeignKey('shipping.id'),
                                   nullable=False)
    date_ordered = db.Column(db.DateTime(timezone=True), nullable=False)
    status_id = db.Column(db.Integer, db.ForeignKey('statuses.id'))
    discount_id = db.Column(db.Integer, db.ForeignKey('discounts.id'))
    billing_customer = db.relationship('Customer')
    billing_address = db.relationship(
        'Address',
        primaryjoin='Order.billing_address_id==Address.id'
    )
    delivery_address = db.relationship(
        'Address',
        primaryjoin='Order.delivery_address_id==Address.id'
    )
    price_currency = db.relationship('Currency')
    shipping_method = db.relationship('Shipping')
    models = db.relationship('OrderProductModel', backref='order',
                             lazy='dynamic')

class OrderProductModel(db.Model):
    __tablename__ = 'order_product_model_maps'
    id = db.Column(db.Integer, primary_key=True)
    order_id = db.Column(db.Integer, db.ForeignKey('orders.id'),
                         nullable=False)
    model_id = db.Column(db.Integer, db.ForeignKey('product_models.id'),
                         nullable=False)
    discount_id = db.Column(db.Integer, db.ForeignKey('discounts.id'))
    tax = db.Column(db.Numeric, nullable=False)
    price = db.Column(db.Numeric, nullable=False)
    price_currency_id = db.Column(db.Integer, db.ForeignKey('currencies.id'),
                                  nullable=False)
    product_qty = db.Column(db.Integer, nullable=False) # TODO: Change to quantity
    subtotal = db.Column(db.Numeric, nullable=False)
    model = db.relationship('ProductModel')
    price_currency = db.relationship('Currency')
