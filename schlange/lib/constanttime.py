def constant_time_eq(a, b):
	# TODO: Document this.
    if len(a) != len(b):
        return False

    result = 0
    for x, y in zip(a, b):
        result |= x ^ y
    return result == 0