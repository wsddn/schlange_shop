import schlange.lib.shopstorage as storage
from tests import SchlangeTestCase

class CartStorageTest(SchlangeTestCase):
    def setUp(self):
        super(CartStorageTest, self).setUp()
        with self.app.test_request_context():
            self.cart = storage.CartStorage()

    def tearDown(self):
        self.cart.delete_all()

    def test_add_get(self):
        self.cart.add(1, 4)
        assert self.cart.get(1) == 4

        self.cart.add(1, 1)
        assert self.cart.get(1) == 5

    def test_get_all(self):
        test_dict = {1:2, 2:3, 5:5, 6:4}
        for uid, qty in test_dict.items():
            self.cart.add(uid, qty)

        assert self.cart.get_all() == test_dict

    def test_update(self):
        self.cart.add(6, 9)
        self.cart.update(6, 1)
        assert self.cart.get(6) == 1

        self.cart.update(6, 0)
        assert self.cart.exists(6) == False

class CheckoutStorageTest(SchlangeTestCase):
    def setUp(self):
        super(CheckoutStorageTest, self).setUp()
        with self.app.test_request_context():
            self.checkout = storage.CheckoutStorage()

    def tearDown(self):
        self.checkout.delete_all()

    def test_add_get(self):
        test_dict = {
            'str': 'collard greens',
            'float': 0.42,
            'int': 23 
        }
        self.checkout.add('test', test_dict)
        assert self.checkout.get('test') == test_dict

    def test_get_all(self):
        test_dict = {
            'satisfaction': {'i': 'cannot', 'get': 'no'},
            'gimmie': ['shelter', 'fade', 'away'],
            'woman': ['american', 'get', 'away', 'from', 'me'],
            'matthew': {'12': 'seek', 'pi': 3.142}
        }
        for key, val in test_dict.items():
            self.checkout.add(key, val)
        print(self.checkout.get_all())
        assert self.checkout.get_all() == test_dict