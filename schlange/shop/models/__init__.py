"""
The shop.models module is one that conatins all SQLAlchemy models and
closly related functions.
"""

# TODO: Put nullable=False for all the NOT NULL fields.

from schlange.shop.models.address import (Address, County, Country, Town)
from schlange.shop.models.currency import (Currency, Discount, Tax)
from schlange.shop.models.customer import (Customer)
from schlange.shop.models.misc import (Image, Status)
from schlange.shop.models.order import (Order, OrderProductModel)
from schlange.shop.models.product import (ProductAttribute, ProductType, \
                                          ProductModel)
from schlange.shop.models.shipping import (Shipping)
