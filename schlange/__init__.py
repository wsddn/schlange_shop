from schlange.data.database import db
from schlange.data.login import login_manager
from flask import Flask
from schlange.shop import init as init_shop
from schlange.user import init as init_user

def create_app(config_filename):
    app = Flask(__name__)
    app.config.from_pyfile(config_filename)

    db.init_app(app)
    login_manager.init_app(app)

    init_shop(app)
    init_user(app)

    return app
