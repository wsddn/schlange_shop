from schlange.data.database import db
from schlange.shop.models.address import Country
from schlange.shop.models.currency import (
    Tax,
    convert_currency,
    format_currency,
    calculate_tax,
    round_currency
)
from schlange.shop.models.misc import Image

class ProductAttribute(db.Model):
    """SQLAlchemy ORM Model, mapping the product_attributes table."""

    __tablename__ = 'product_attributes'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256), nullable=False)
    value = db.Column(db.String(256), nullable=False)

class ProductType(db.Model):
    """SQLAlchemy ORM Model, mapping the product_type table."""

    __tablename__ = 'product_types'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256), nullable=False)
    description = db.Column(db.Text)

product_model_attribute_maps = db.Table('product_model_attribute_maps',
    db.Column('model_id', db.Integer, db.ForeignKey('product_models.id')),
    db.Column('attribute_id', db.Integer,
        db.ForeignKey('product_attributes.id')
    )
)

product_model_country_maps = db.Table('product_model_country_maps',
    db.Column('model_id', db.Integer, db.ForeignKey('product_models.id')),
    db.Column('country_id', db.Integer, db.ForeignKey('countries.id'))
)

# TODO: Should be a class? See http://bit.ly/1jVu2fG
product_model_image_maps = db.Table('product_model_image_maps',
    db.Column('model_id', db.Integer, db.ForeignKey('product_models.id')),
    db.Column('image_id', db.Integer, db.ForeignKey('images.id')),
    db.Column('image_order', db.Integer),
)

product_model_shipping_maps = db.Table('product_model_shipping_maps',
    db.Column('model_id', db.Integer, db.ForeignKey('product_models.id')),
    db.Column('shipping_id', db.Integer, db.ForeignKey('shipping.id'))
)

product_model_tax_maps = db.Table('product_model_tax_maps',
    db.Column('model_id', db.Integer, db.ForeignKey('product_models.id')),
    db.Column('tax_id', db.Integer, db.ForeignKey('taxes.id'))
)


class ProductModel(db.Model):
    """SQLAlchemy ORM Model, mapping the product_model table."""

    __tablename__ = 'product_models'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256), nullable=False)
    description = db.Column(db.Text)
    price = db.Column(db.Numeric, nullable=False)
    price_currency_id = db.Column(db.Integer,
        db.ForeignKey('currency.id'))
    cost = db.Column(db.Numeric)
    mass = db.Column(db.Float)
    height = db.Column(db.Float)
    width = db.Column(db.Float)
    type_id = db.Column(db.Integer, db.ForeignKey('product_types.id'),
        nullable=False)
    discount_id = db.Column(db.Integer, db.ForeignKey('discounts.id'))
    stock = db.Column(db.Integer, nullable=False)
    product_type = db.relationship('ProductType',
        backref=db.backref('models', lazy='dynamic')
    )
    attributes = db.relationship('ProductAttribute',
        secondary=product_model_attribute_maps,
        backref=db.backref('products', lazy='dynamic')
    )
    countries = db.relationship('Country',
        secondary=product_model_country_maps
    )
    shipping_methods = db.relationship('Shipping',
        secondary=product_model_shipping_maps,
        backref=db.backref('products', lazy='dynamic')
    )
    images = db.relationship('Image',
        secondary=product_model_image_maps
    )
    taxes = db.relationship('Tax',
        secondary=product_model_tax_maps,
        backref=db.backref('products', lazy='dynamic')
    )

    @property
    def sorted_images(self):
        return Image.query.join(product_model_image_maps).filter(
            product_model_image_maps.c.model_id == self.id
        ).order_by(product_model_image_maps.c.image_order.asc()).all()

    def get_taxes(self, country_id):
        """Return a list of taxes for this product_model and the
        given country."""
        # TODO: Should this be a getter?
        # TODO: Change name.
        # TODO: Improve documentation.
        return Tax.query.\
            outerjoin((Tax.products, ProductModel)).\
            outerjoin((Tax.countries, Country)).\
            filter(
                (ProductModel.id == self.id) |
                (Country.id == country_id)).\
            all()


    def price_no_tax(self, to_id):
        """Calculate the price of a ProductModel without tax.

        Args:
            to_id: ID of the currency to convert the price into.

        Returns:
            The ProductModel price, converted into given currency without tax
        """
        # TODO: Implement basic caching of this in the class (saves db lookups)

        return round_currency(
            convert_currency(self.price, self.price_currency_id, to_id),
            to_id
        )

    def net_price(self, to_id, country_id):
        """Calculate the net price of a ProductModel.

        Calculate the net price, including tax and currency conversion, of a
        ProductModel.

        Args:
            to_id: Currency id that the price should be converted into.
            country_id: Country id used to calculate tax with.

        Returns:
            A float with the final net price of the ProductModel.
        """

        # TODO: Cache the price.
        price = self.price_no_tax(to_id)

        # Get all applicable taxes (based upon country and product model)
        taxes = self.get_taxes(country_id)
        return round_currency(price + calculate_tax(price, to_id, taxes),
                              to_id)

    def net_format_price(self, to_id, country_id):
        net = self.net_price(to_id, country_id)
        return format_currency(net, to_id)
