from flask import (Blueprint, render_template, request, redirect, url_for)
from flask.ext.login import login_user, logout_user, login_required

from schlange.user.models.user import User
from schlange.user.forms import LoginForm

blueprint = Blueprint('login', __name__, template_folder='templates',
                      static_folder='static')


# TODO: Rename these routes.
@blueprint.route('/in', methods=('GET', 'POST'))
def login():
    form = LoginForm()
    0/0

    if form.validate_on_submit():
        # TODO: Where to redirect?
        # TODO: Have a r parameter (r for redirect).
        # TODO: Maybe remove the force, for login_user
        # TODO: 
        user = User.query.filter(User.email == form.email.data).first()
        succeeded = login_user(user, form.remember.data, True)

    return render_template('login.html', form=form)

@blueprint.route('/out', methods=('GET', 'POST'))
@login_required
def logout():
    logout_user()
    return render_template('logout.html')