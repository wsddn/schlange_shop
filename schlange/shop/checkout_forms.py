"""
This module defines the WTForms used for the checkout process.
"""

from flask_wtf import Form
from wtforms.fields import (BooleanField, PasswordField, RadioField,
                            SelectField, StringField, SubmitField)
from wtforms.fields.html5 import EmailField
from wtforms.validators import EqualTo, InputRequired, Optional

from schlange.user.forms import EmailUniqueInDb, LoginForm
from schlange.shop.models.address import Country
from schlange.shop.models.customer import MALE_TITLES, FEMALE_TITLES

ALL_TITLES = [(t, t.title()) for t in MALE_TITLES+FEMALE_TITLES]

class StartLoginForm(LoginForm):
    submit = SubmitField('Login')

    def submitted_validated(self):
        # TODO: Is using this function right
        # TODO: Should sumbit.data be checked before validate
        return self.is_submitted() and self.submit.data and self.validate()

class StartChoiceForm(Form):
    choice = RadioField(
        'Checkout as guest or register:',
        [InputRequired()],
        choices=[
            ('register', 'Register'),
            ('guest', 'Guest')
        ],
        coerce=str,
    )
    submit = SubmitField('Continue')

    def submitted_validated(self):
        return self.is_submitted() and self.submit.data and self.validate()

# TODO: Is this a wise idea using this validator for billing where its 
# not even needed.
class RequireOnDifferentAddress(InputRequired):
    def __call__(self, form, field):
        if hasattr(form, 'same_address') and not form.same_address.data:
            super(InputRequired, self).__call__(form, field)

class BaseAddressForm(Form):
    """Base class for forms collecting addresses."""
    title = SelectField('Title', choices=ALL_TITLES,
                        validators=[RequireOnDifferentAddress()])
    first_name = StringField('First Name', [RequireOnDifferentAddress()])
    last_name = StringField('Last Name', [RequireOnDifferentAddress()])
    address_line_1 = StringField('Line 1', [RequireOnDifferentAddress()])
    address_line_2 = StringField('Line 2', [Optional()])
    address_line_3 = StringField('Line 3', [Optional()])
    address_town = StringField('Town', [RequireOnDifferentAddress()])
    address_county = StringField('County', [RequireOnDifferentAddress()])
    address_postcode = StringField('Postcode', [RequireOnDifferentAddress()])
    address_country = SelectField('Country', coerce=int,
                                  validators=[RequireOnDifferentAddress()])

class BillingForm(BaseAddressForm):
    """Represents the billing form, the form for the billing step of checkout.
    Inherits from BaseAddressForm.
    """
    email = EmailField('Email Address', [InputRequired()])
    tos = BooleanField('I accept the terms of service', [InputRequired()])

class RegisterBillingForm(BaseAddressForm):
    email = EmailField('Email Address', [InputRequired(), EmailUniqueInDb])
    password = PasswordField('Password', [
        InputRequired(),
        EqualTo('confirm_password', message='Passwords must match')
    ])
    confirm_password = PasswordField('Repeat Password')
    tos = BooleanField('I accept the terms of service', [InputRequired()])

class DeliveryForm(BaseAddressForm):
    """Represents the delivery form, for the delivery address
    step of checkout."""
    # FIXME: Get WTForms to allow booleans.
    same_address = RadioField(
        'Where to ship:',
        choices=[
            (1, 'Ship to billing address'),
            (0, 'Ship to different address')
        ],
        coerce=int,
        default=1
    )


class MethodForm(Form):
    """Represents the form for the shipping method step of checkout."""
    method = SelectField('Shipping Method', coerce=int)


class ConfirmForm(Form):
    """Represents the form for the confirm step of checkout."""
    confirm = SubmitField('Look\'s fine')
    back = SubmitField('Go back')
