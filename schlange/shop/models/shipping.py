from sqlalchemy.sql import func

from schlange.data.database import db
from schlange.shop.models.address import Country
from schlange.shop.models.product import ProductModel
from schlange.shop.models.currency import (
    Tax,
    convert_currency,
    format_currency,
    calculate_tax,
    round_currency
)

shipping_country_maps = db.Table('shipping_country_maps',
    db.Column('shipping_id', db.Integer, db.ForeignKey('shipping.id')),
    db.Column('country_id', db.Integer, db.ForeignKey('countries.id'))
)

shipping_tax_maps = db.Table('shipping_tax_maps',
    db.Column('shipping_id', db.Integer, db.ForeignKey('shipping.id')),
    db.Column('tax_id', db.Integer, db.ForeignKey('taxes.id'))
)

class Shipping(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256), nullable=False)
    price = db.Column(db.Numeric, nullable=False)
    price_currency_id = db.Column(db.Integer,
        db.ForeignKey('currencies.id'), nullable=False)
    description = db.Column(db.Text)
    min_mass = db.Column(db.Float)
    max_mass = db.Column(db.Float)
    min_width = db.Column(db.Float)
    max_width = db.Column(db.Float)
    min_height = db.Column(db.Float)
    max_height = db.Column(db.Float)
    min_length = db.Column(db.Float)
    max_length = db.Column(db.Float)
    countries = db.relationship('Country',
        secondary=shipping_country_maps
    )
    taxes = db.relationship('Tax',
        secondary=shipping_tax_maps,
        backref=db.backref('shipping', lazy='dynamic')
    )
    price_currency = db.relationship('Currency')

    def price_no_tax(self, to_id):
        """Calculate the price of a Shipping without tax.

        Args:
            to_id: ID of the currency to convert the price into.

        Returns:
            The Shipping price, converted into given currency without tax
        """
        # TODO: Implement basic caching of this in the class (saves db lookups)

        return round_currency(
            convert_currency(self.price, self.price_currency_id, to_id),
            to_id
        )

    def net_price(self, to_id, country_id):
        """Calculate the net price of a Shipping.

        Calculate the net price, including tax and currency conversion, of a
        Shipping.

        Args:
            to_id: Currency id that the price should be converted into.
            country_id: Country id used to calculate tax with.

        Returns:
            A float with the final net price of the Shipping.
        """

        # TODO: Cache the price.
        price = self.price_no_tax(to_id)

        # Get all applicable taxes (based upon country and shipping)
        taxes = Tax.query.\
            outerjoin((Tax.shipping, Shipping)).\
            outerjoin((Tax.countries, Country)).\
            filter(
                (Shipping.id == self.id) |
                (Country.id == country_id)).\
            all()

        return round_currency(
            price + calculate_tax(price, to_id, taxes),
            to_id
        )

    def net_format_price(self, to_id, country_id):
        net = self.net_price(to_id, country_id)
        return format_currency(net, to_id)

def shipping_from_cart(cart, country_id):
    """Takes a dict {model_id, quantity} which represents the user's cart and
    the country id that the user wishes to ship to."""
    # TODO: Implement
    # TODO: Change name.

    cart_subquery = db.session.query(
        func.unnest(list(cart.keys())).label('model_id'),
        func.unnest(list(cart.values())).label('quantity')
    ).subquery()

    # TODO: Simplify
    # TODO: Document
    mass = func.sum(
        ProductModel.mass * cart_subquery.c.quantity
    )

    shipping_methods = Shipping.query.\
        join((Shipping.products, ProductModel)).\
        join((Shipping.countries, Country)).\
        join(cart_subquery, ProductModel.id == cart_subquery.c.model_id).\
        filter(
            Country.id == country_id
        ).\
        group_by(Shipping.id).\
        having(
            mass.between(Shipping.min_mass, Shipping.max_mass) |
            (mass == None) |
            ((Shipping.min_mass == None) |
            (Shipping.max_mass == None))
        ).\
        all()

    return shipping_methods
