"""
This module implements a session based storage of a user's country and
currency.

Proprietary.
"""
from flask import request, session
from pygeoip import GeoIP, GeoIPError
from schlange.shop.models.address import Country

class Localisation(object):
    """Provides easy access and guessing for the user's country and
    currency id"""

    COUNTRY_NAME = 'country'
    CURRENCY_NAME = 'currency'

    def __init__(self, app=None):
        # TODO: Make this accept app
        # TODO: Set these to none.
        self.default_country = 1
        self.default_currency = 1
        self.geoip_dat_location = 'data/geoip.dat'

        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        self.default_country = app.config.get('LOCALISATION_DEFAULT_COUNTRY')
        self.default_currency = app.config.get('LOCALISATION_DEFAULT_CURRENCY')
        self.geoip_dat_location = app.config.get('LOCALISATION_GEOIP_LOCATION')
    
    @property
    def country_id(self):
        """Returns the current user's country id."""
        if self.COUNTRY_NAME in session:
            return session[self.COUNTRY_NAME]

        try:
            geoip = GeoIP(self.geoip_dat_location)
            code = geoip.country_code_by_addr(request.remote_addr)
            country = Country.query.filter(Country.code == code).first()
            if country is None:
                country_id = self.default_country
            else:
                country_id = country.id
        except (IOError, GeoIPError):
            country_id = self.default_country

        session[self.COUNTRY_NAME] = country_id
        return country_id

    @country_id.setter
    def country_id(self, country_id):
        """Set the user's country id."""
        # TODO: Check if country exists.
        session[self.COUNTRY_NAME] = country_id

    @property
    def currency_id(self):
        """Returns the current user's currency id."""
        if self.CURRENCY_NAME in session:
            return session[self.CURRENCY_NAME]

        if self.COUNTRY_NAME in session:
            country = Country.query.get(session[self.COUNTRY_NAME])
            if country is not None:
                session[self.CURRENCY_NAME] = country.currency_id
                return country.currency_id

        # TODO: Call country() to get country, and try default currency.
        return self.default_currency    

    @currency_id.setter
    def currency_id(self, currency_id):
        """Set the user's currency id."""
        # TODO: Check if currency exists.
        session[self.CURRENCY_NAME] = currency_id
