from sqlalchemy.sql.expression import ColumnElement
#from sqlalchemy.dialects import postgresql
from sqlalchemy.ext.compiler import compiles

class array_agg(ColumnElement):
    def __init__(self, expr, order_by=None):
        #self.type = Text()
        self.expr = expr
        self.order_by = order_by

    @property
    def _from_objects(self):
        return self.expr._from_objects

@compiles(array_agg, 'postgresql')
def compile_array_agg(element, compiler, **kwargs):
    head = 'array_agg(%s' % (
        compiler.process(element.expr),
    )

    if element.order_by is not None:
        tail = ' ORDER BY %s)' % compiler.process(element.order_by)
    else:
        tail = ')'
    return head + tail
