from schlange.data.database import db

class Address(db.Model):
    __tablename__ = 'addresses'
    id = db.Column(db.Integer, primary_key=True)
    line_1 = db.Column(db.String(512), nullable=False)
    line_2 = db.Column(db.String(512))
    line_3 = db.Column(db.String(512))
    town_id = db.Column(db.Integer, db.ForeignKey('towns.id'))
    county_id = db.Column(db.Integer, db.ForeignKey('counties.id'))
    postcode = db.Column(db.String(10))
    country_id = db.Column(db.Integer, db.ForeignKey('countries.id'))

class County(db.Model):
    __tablename__ = 'counties'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    addresses = db.relationship('Address', backref='county', lazy='dynamic')

class Country(db.Model):
    __tablename__ = 'countries'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    code = db.Column(db.String(2), nullable=False)
    currency_id = db.Column(db.Integer, db.ForeignKey('currencies.id'))
    addresses = db.relationship('Address', backref='country', lazy='dynamic')

class Town(db.Model):
    __tablename__ = 'towns'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(512), nullable=False)
    addresses = db.relationship('Address', backref='town', lazy='dynamic')


def standardise_address(address):
    address['line_1'] = address['line_1'].title()
    if address['line_2'] is not None:
        address['line_2'] = address['line_2'].title()
    if address['line_3'] is not None:
        address['line_3'] = address['line_3'].title()
    address['town_name'] = address['town_name'].title()
    address['county_name'] = address['county_name'].title()
    address['postcode'] = address['postcode'].upper()

def insert_address(data):
    standardise_address(data)

    address = Address(
        line_1=data['line_1'],
        line_2=data['line_2'],
        line_3=data['line_3'],
        postcode=data['postcode'],
        country_id=data['country_id']
    )

    town = Town.query.filter(Town.name == data['town_name']).first()
    if town is None:
        town = Town(name=data['town_name'])
        db.session.add(town)
        db.session.commit()
    address.town_id = town.id

    county = County.query.filter(County.name == data['county_name']).first()
    if county is None:
        county = County(name=data['county_name'])
        db.session.add(county)
        db.session.commit()
    address.county_id = county.id

    db.session.add(address)
    db.session.commit()

    return address

def get_address(data):
    standardise_address(data)
    return Address.query.\
        join(Town).\
        join(County).\
        filter(
            Address.line_1 == data['line_1'],
            Address.line_2 == data['line_2'],
            Address.line_3 == data['line_3'],
            Town.name == data['town_name'],
            County.name == data['county_name'],
            Address.postcode == data['postcode'],
            Address.country_id == data['country_id']
        ).first()

def get_insert_address(data):
    address = get_address(data)
    if address is None:
        address = insert_address(data)
    return address
