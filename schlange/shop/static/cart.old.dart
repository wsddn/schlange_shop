import 'dart:convert';
import 'dart:html';
import 'dart:async';

void main() {
	handleQtyChanges();
}

void handleQtyChanges() {
	ElementList qtyInputs = document.querySelectorAll('.cart-qty');
	StreamSubscription sub = qtyInputs.onChange.listen((e) {
		InputElement itemElem = e.currentTarget;
		while (itemElem.className != 'cart-item') {
			itemElem = itemElem.parent;
		}
		String modelId = itemElem.parent.dataset['id'];

		HttpRequest.postFormData('/shop/cart/update', {
			'product': modelId,
			'quantity': e.currentTarget.value
		});
		HttpRequest.getString('/shop/cart/get').then(updateCart);
	});
}

void updateCart(String jsonResponce) {
	Map cartData = JSON.decode(jsonResponce);
	Map productList = cartData['products'];
	Element cart = document.querySelector('#cart-view');
	ElementList cartItems = cart.querySelectorAll('.cart-item');
	
	// Go through all elements in the cart, and update them.
	cartItems.forEach((Element listItem) {
		if (productList.containsKey(listItem.dataset['id'])) {
			// This product exists
			Map productData = productList[listItem.dataset['id']];
			listItem.querySelector('.cart-subtotal').setInnerHtml(productData['subtotal'].toString());
		} else {
			// This product has been deleted so remove it from the DOM
			listItem.remove();
		}
	});

	// If there are no more items in the cart refresh the page.
	if (productList.length == 0) {
		window.location.assign(window.location.href);
	}
}