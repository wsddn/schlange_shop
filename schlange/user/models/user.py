import hashlib
import scrypt

from binascii import hexlify
from os import urandom

from schlange.data.database import db
from schlange.lib.constanttime import constant_time_eq

def generate_code():
    # TODO: Document this.
    return hashlib.sha512(urandom(32)).hexdigest()

def generate_salt():
    # TODO: Document this
    return hexlify(urandom(32)).decode('utf-8')

def hash_password(salt, password):
    # TODO: Document this.
    # TODO: Make the password forwards compatible.
    # TODO: Should a scrypt.error exception raise a type error?
    try:
        hashed_pw = hexlify(scrypt.hash(password, salt, 1 << 14, 8, 1, 64))
    except scrypt.error:
        raise TypeError
    return hashed_pw.decode('utf-8')

class UserCode(db.Model):
    __tablename__ = 'user_codes'
    id = db.Column(db.Integer, primary_key=True)
    date_start = db.Column(db.DateTime(timezone=True), nullable=False)
    date_end = db.Column(db.DateTime(timezone=True), nullable=False)
    code = db.Column(db.String(128), nullable=False)
    type = db.Column(db.String(64), nullable=False)
    used = db.Column(db.Boolean, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'),
                        nullable=False)

class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), nullable=False)
    password = db.Column(db.String(128), nullable=False)
    salt = db.Column(db.String(32), nullable=False)
    date_created = db.Column(db.DateTime(timezone=True), nullable=False)
    active = db.Column(db.Boolean, default=False, nullable=False)
    customers = db.relationship('Customer', backref='user', lazy='dynamic')
    codes = db.relationship('UserCode', backref='user', lazy='dynamic')

    def check_password(self, password):
        # TODO: Decide how to store the password, as bytes or str.
        hashed_pw = hash_password(self.salt, password).encode('utf-8')
        user_pw = self.password.encode('utf-8')
        return constant_time_eq(hashed_pw, user_pw)

    # TODO: Document these, say its for Flask-Login
    def is_active(self):
        return self.active

    # TODO: Implement this, maybe a _authenticated field.
    def is_authenticated(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id
