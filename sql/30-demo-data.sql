-- Demo data insertion script.
-- TODO: Find out the proper order of which table to add.

-- Addresses

INSERT INTO countries (id, name, code, currency_id) VALUES
(1, 'United Kingdom', 'UK', 1);

-- Currencies + Money

INSERT INTO currencies (id, name, symbol, code, decimal_places) VALUES
(1, 'Pound Sterling', '£', 'GBP', 2);

INSERT INTO payment_processors (id, name, display_name, default_processor_currency_id) VALUES
(1, 'firstdata', 'First Data', 1);

INSERT INTO payment_processor_currencies (id, name, payment_processor_id, currency_id) VALUES
(1, '826', 1, 1);

INSERT INTO taxes (id, name, percentage, fixed) VALUES
(1, 'VAT', 20, NULL);

INSERT INTO tax_country_maps (id, tax_id, country_id) VALUES
(1, 1, 1);

-- INSERT INTO currency_conversion (id, currency_from_id, currency_to_id, rate) VALUES
-- (1, 1, 2, 1.5);

-- Images 

INSERT INTO images (id, path, thumbnail_path, alt, width, height) VALUES
(1, 'blackYellowSingleCooker.png', NULL, NULL, NULL, NULL),
(2, 'smallBlackYellowSingleCooker.png', NULL, NULL, NULL, NULL);

-- Shipping

INSERT INTO shipping (id, name, price, price_currency_id, description, min_mass, max_mass, min_width, max_width, min_height, max_height) VALUES
(1, 'Standard Royal Mail Shipping', 4.17, 1, '~12 weeks', NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO shipping_country_maps (id, country_id, shipping_id) VALUES
(1, 1, 1);

-- Products

INSERT INTO product_types (id, name, description) VALUES
(1, 'Single Egg Cooker', NULL);

-- INSERT INTO product_attributes (id, name, value) VALUES
-- (1, 'Colour', 'Red'),
-- (2, 'Plug', 'UK'),
-- (3, 'Colour', 'Blue'),
-- (4, 'Plug', 'US');

INSERT INTO product_models (id, name, description, price, price_currency_id, cost, mass, height, width, type_id, discount_id, stock) VALUES
(1, 'Black and Yellow Single Egg Cooker', 'Market Acceptance Test water free egg cooker for one medium to large egg (55g-80g). This limited edition cooker has a UK plug, requires a 240V AC supply, and has a black body and yellow trim.', 20.825, 1, NULL, NULL, NULL, NULL, 1, NULL, 100);
-- (2, 'Blue Single Egg Cooker', NULL, 24.99, 1, NULL, NULL, NULL, NULL, 1, NULL, 16);

-- INSERT INTO product_model_attributes (id, model_id, attribute_id) VALUES
-- -(1, 1, 1),
-- (2, 1, 2),
-- (3, 2, 3),
-- (4, 2, 4);

INSERT INTO product_model_country_maps (id, model_id, country_id) VALUES
(1, 1, 1);
-- (2, 2, 1);

INSERT INTO product_model_image_maps (id, model_id, image_id, image_order) VALUES
(1, 1, 1, 1),
-- (2, 2, 2, 1, 0),
(2, 1, 2, 2);
-- (4, 2, 4, 0, 1);

INSERT INTO product_model_shipping_maps (id, model_id, shipping_id) VALUES
(1, 1, 1);
-- (2, 2, 1);
