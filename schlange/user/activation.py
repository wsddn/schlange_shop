import hashlib

from datetime import datetime

from flask import (Blueprint, render_template, request, redirect, url_for)

from schlange.data.database import db
from schlange.lib.email import send_normal_email
from schlange.user.forms import ActivationForm
from schlange.user.models.user import UserCode

blueprint = Blueprint('activation', __name__, template_folder='templates',
                      static_folder='static')

@blueprint.route('/', methods=('GET', 'POST'))
def activate():
    form = ActivationForm()

    if form.validate_on_submit():
        # TODO: Remove when debugging 
        register_user(form.email.data, form.password.data)
        return redirect(url_for('register.done'))

    return render_template('activation/activate_form.html', form=form)

@blueprint.route('/<code>')
def activate_with_code(code):
	user_code = UserCode.query.filter(
		UserCode.code == code,
		UserCode.type == 'activation',
		UserCode.used == False,
		(UserCode.date_start >= datetime.now() | UserCode.date_start == None),
		(UserCode.date_end < datetime.now() | UserCode.date_end == None)
	).first()

	if user_code is None:
		return render_template('activation/no_code.html')

	user = user_code.user

	user.active = True
	user_code.used = True
	db.commit.commit()

	return render_template('activation/done.html', user=user)


def generate_activation_code(user):
	#ACTIVATION_TIME 
	code = UserCode(

	)
    pass

def send_activation_email(user):
    # TODO: See if the db table for this can be used for recovering passwords.
    SENDER_EMAIL = 'info@eggxactly.com'



    pass