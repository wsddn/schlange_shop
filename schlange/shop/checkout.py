"""
The checkout module of the shop implements a front-end to a Redis based
checkout data holder. It uses WTForms to render the forms and parse the
submitted data.
"""

# TODO: Decide if it is wise to put payment functions here.
from datetime import datetime

from flask import (abort, Blueprint, current_app, redirect, render_template,
                   request, session, url_for)
from flask.ext.login import login_user

from schlange.data.database import db
from schlange.lib.shopstorage import (CheckoutStorage, CartStorage)
from schlange.lib.payment import payment_processor
from schlange.lib.localisation import Localisation
from schlange.shop.cart import get_cart, get_raw_cart
from schlange.shop.checkout_forms import (
    StartLoginForm,
    StartChoiceForm,
    BillingForm,
    RegisterBillingForm,
    ConfirmForm,
    DeliveryForm,
    MethodForm
)
from schlange.shop.models.address import Country, get_insert_address
from schlange.shop.models.currency import (
    PaymentProcessor,
    round_currency,
    format_currency,
    calculate_tax
)
from schlange.shop.models.customer import (get_customer, Customer,
                                           gender_from_title)
from schlange.shop.models.order import Order, OrderProductModel
from schlange.shop.models.shipping import Shipping, shipping_from_cart
from schlange.user.models.user import User
from schlange.user.register import register_user

blueprint = Blueprint('checkout', __name__, template_folder='templates',
                      static_folder='static')


@blueprint.route('/start', methods=('GET', 'POST'))
def start():
    """Display, validate and process the start method stage of checkout."""

    # TODO: Tell the user that they need to have added things to cart
    if not CartStorage():
        abort(403)

    login_form = StartLoginForm(prefix='login_form')
    choice_form = StartChoiceForm(prefix='choice_form')
    checkout = CheckoutStorage()

    if login_form.submitted_validated():
        # TODO: Get user from form.
        # TODO: Remove the force from the login.
        user = User.query.filter(User.email == form.email.data).first()
        login_user(user, login_form.remember.data, True)
        checkout['start'] = 'login'
        return redirect(url_for('checkout.billing'))

    if choice_form.submitted_validated():
        checkout['start'] = choice_form.choice.data
        return redirect(url_for('checkout.billing'))

    return render_template('checkout/start.html', login_form=login_form,
                           choice_form=choice_form)

@blueprint.route('/billing', methods=('GET', 'POST'))
def billing():
    """Display, validate and process the billing address stage of checkout."""

    checkout = CheckoutStorage()
    if checkout['start'] == 'login':
        form = BillingForm()
    elif checkout['start'] == 'guest':
        print('guest')
        form = BillingForm()
    elif checkout['start'] == 'register':
        print('register')
        form = RegisterBillingForm()
   
    form.address_country.choices = [(c.id, c.name) 
                                    for c in Country.query.all()]

    if form.validate_on_submit():
        # TODO: Set the locale country from this.
        checkout.add('billing', form.data)
        return redirect(url_for('checkout.delivery'))

    return render_template('checkout/billing.html', action=checkout['start'],
                           form=form)


@blueprint.route('/delivery', methods=('GET', 'POST'))
def delivery():
    """Display, validate and process the delivery address stage of checkout."""
    checkout = CheckoutStorage()
    form = DeliveryForm()
    form.address_country.choices = [(c.id, c.name) 
                                    for c in Country.query.all()]
    if not checkout.exists(['billing']):
        abort(403)

    if form.validate_on_submit():

        # If delivery address is same as billing address
        # then set delivery data to be equal to billing data.
        if form.same_address.data:
            for field, data in checkout.get('billing').items():
                try:
                    form[field].data = data
                except KeyError:
                    continue

        checkout.add('delivery', form.data)
        return redirect(url_for('checkout.method'))

    return render_template('checkout/delivery.html', form=form)


@blueprint.route('/method', methods=('GET', 'POST'))
def method():
    """Display, validate and process the shipping method stage of checkout."""

    checkout = CheckoutStorage()
    form = MethodForm()

    if not checkout.exists(['billing', 'delivery']):
        abort(403)

    cart_data = CartStorage().get_all()
    shipping_methods = shipping_from_cart(cart_data, checkout['delivery']['address_country'])
    form.method.choices = [(m.id, m.name) for m in shipping_methods]

    if form.validate_on_submit():
        checkout.add('method', form.method.data)
        return redirect(url_for('checkout.confirm'))

    return render_template('checkout/method.html', form=form)


@blueprint.route('/confirm', methods=('GET', 'POST'))
def confirm():
    """Display, validate and process the confirm stage of checkout."""
    form = ConfirmForm()
    checkout = CheckoutStorage()
    cart_data = get_cart()
    locale = Localisation()

    if not checkout.exists(['start', 'billing', 'delivery', 'method']):
        abort(403)

    if form.is_submitted():
        if form.data['confirm']:
            checkout.add('confirm', True)
            return redirect(url_for('checkout.payment'))
        elif form.data['back']:
            return redirect(url_for('checkout.method'))

    shipping_method = Shipping.query.get(checkout.get('method'))
    shipping_price = shipping_method.net_price(locale.currency_id,
                                               locale.country_id)
    cart_data['products']['shipping'] = {
        'name': shipping_method.name,
        'img_src': '',
        'quantity': 1,
        'price': format_currency(shipping_price, locale.currency_id),
        'subtotal': format_currency(shipping_price, locale.currency_id),
    }
    cart_data['total_raw'] += shipping_method.price
    cart_data['total'] = format_currency(cart_data['total_raw'],
                                         locale.currency_id)

    return render_template('checkout/confirm.html', cart=cart_data, form=form)


@blueprint.route('/payment')
def payment():
    """Display the payment stage of checkout."""
    # TODO: Make the stages constant, look for nicer way.
    # TODO: Turn all of this into seperate functions.
    # TODO: Instead of doing everything monlithically, do db entry step by step.

    checkout = CheckoutStorage()
    checkout_data = checkout.get_all()
    cart_data = get_raw_cart()
    locale = Localisation()
    # TODO: Allow changing the payment processor.
    processor = payment_processor('firstdata')(current_app)

    checkout_stages = ['start', 'billing', 'delivery', 'method', 'confirm']
    if not checkout.exists(checkout_stages):
        abort(403)

    # Order already exists
    # TODO: Document the assumptions of this.
    if 'order_id' in session:
        order = Order.query.get(session['order_id'])
        # TODO: Remove this. Why is this here?
        order = None

    # Need to create order.
    if 'order_id' not in session or order is None:
        # TODO: Implement adding customers.

        if checkout_data['start'] == 'register':
            # Register the user.
            user = register_user(checkout_data['billing']['email'],
                                 checkout_data['billing']['password'])

        billing_address = get_insert_address({
            'line_1': checkout_data['billing']['address_line_1'],
            'line_2': checkout_data['billing']['address_line_2'],
            'line_3': checkout_data['billing']['address_line_3'],
            'town_name': checkout_data['billing']['address_town'],
            'county_name': checkout_data['billing']['address_county'],
            'postcode': checkout_data['billing']['address_postcode'],
            'country_id': checkout_data['billing']['address_country']
        })
        delivery_address = get_insert_address({
            'line_1': checkout_data['delivery']['address_line_1'],
            'line_2': checkout_data['delivery']['address_line_2'],
            'line_3': checkout_data['delivery']['address_line_3'],
            'town_name': checkout_data['delivery']['address_town'],
            'county_name': checkout_data['delivery']['address_county'],
            'postcode': checkout_data['delivery']['address_postcode'],
            'country_id': checkout_data['delivery']['address_country']
        })

        customer = get_customer(checkout_data['billing']['first_name'],
                                checkout_data['billing']['last_name'],
                                checkout_data['billing']['email'])
        if customer is None:
            customer = Customer(
                title=checkout_data['billing']['title'],
                first_name=checkout_data['billing']['first_name'],
                first_name_lower=checkout_data['billing']['first_name'].lower(),
                last_name=checkout_data['billing']['last_name'],
                last_name_lower=checkout_data['billing']['last_name'].lower(),
                email=checkout_data['billing']['email'].lower(),
                gender=gender_from_title(checkout_data['billing']['title'])
            )
            if checkout_data['start'] == 'register':
                customer.user_id = user.id
            db.session.add(customer)
            db.session.commit()

        customer.add_address(billing_address.id, 'billing')
        customer.add_address(delivery_address.id, 'delivery')

        total_price = sum(
            model.net_price(locale.currency_id, locale.country_id) * qty
            for (model, qty) in cart_data
        )
        total_price += Shipping.query.get(checkout_data['method']).\
            net_price(locale.currency_id, locale.country_id)

        order = Order(
            billing_customer_id=customer.id,
            billing_address_id=billing_address.id,
            delivery_first_name=checkout_data['delivery']['first_name'],
            delivery_last_name=checkout_data['delivery']['last_name'],
            delivery_address_id=delivery_address.id,
            price=total_price,
            price_currency_id=locale.currency_id,
            date_ordered=datetime.now(),
            shipping_method_id=checkout_data['method'],
            completed=False,
            failed=False,
            payment_processor_id=processor.id
        )

        db.session.add(order)
        db.session.commit()

        for (model, quantity) in cart_data:
            model_price = model.net_price(locale.currency_id,
                                          locale.country_id)
            tax = calculate_tax(model_price, locale.currency_id,
                                model.get_taxes(locale.country_id))
            db.session.add(OrderProductModel(
                order_id=order.id,
                model_id=model.id,
                tax=round_currency(tax, locale.currency_id),
                price=model_price,
                price_currency_id=locale.currency_id,
                product_qty=quantity,
                subtotal=round_currency(model_price * quantity,
                                        locale.currency_id)
            ))
        db.session.commit()
        session['order_id'] = order.id

    
    # If payment gateway wishes to redirect the user then redirect.
    # Else display form for card details.
    if processor.REDIRECT is False:
        # TODO: Remove this.
        recieve_url = 'http://eggxactly.com/fdgetter.php'
        #url_for('checkout.receive', processor=processor.NAME, _external=True)
        form = processor.render_form(order, recieve_url)

        return render_template('checkout/payment.html', rendered_form=form)
    else:
        return redirect(processor.REDIRECT_URL)


@blueprint.route('/receive', methods=('GET', 'POST'))
def recieve():
    """Receive and process data from the payment provider,
    returning a status page"""

    try:
        processor = payment_processor(request.args['processor'])(current_app)
    except TypeError:
        abort(500)

    result = processor.handle_responce(request)

    # Order id has not been given, but recovery is possible.
    # TODO: Can we even get here?
    if result.order is None and 'order_id' in session:
        result.order = Order.query.get(session['order_id'])

    # If no fatal error occured then handle the result normally.
    has_account = CheckoutStorage().get('start') in ['login', 'register']
    if result.success:
        result.order.completed = True
        result.order.failed = False
        session.pop('order_id', None)
        CartStorage().delete_all()
        CheckoutStorage().delete_all()
    elif not result.success and result.order is not None:
        result.order.completed = False
        result.order.failed = True
        result.order.processor_detail = result.detail

    db.session.commit()
    return render_template('checkout/recieve.html', result=result,
                           has_account=has_account)
