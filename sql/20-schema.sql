-- Database Schema for eggxactly.ltd
-- Copyright eggxactly.ltd 2014
-- All Rights Reserved

-- TODO: Use foreign keys
-- TODO: Correct and improve constraint names.
-- TODO: Document tables
-- TODO: Document fields.
 
--
-- The Addresses table
-- 
-- Holds all addresses with some commonly repeated data like countries in separate tables
--

CREATE TABLE IF NOT EXISTS addresses (
  id SERIAL PRIMARY KEY,
  line_1 VARCHAR(512) NOT NULL,
  line_2 VARCHAR(512) NULL,
  line_3 VARCHAR(512) NULL,
  town_id INTEGER NOT NULL,
  county_id INTEGER NULL,
  postcode VARCHAR(10) NULL,
  country_id INTEGER NOT NULL /*,
  CONSTRAINT FK_addresses_town_id FOREIGN KEY (town_id) REFERENCES towns(id),
  CONSTRAINT FK_addresses_county_id FOREIGN KEY (county_id) REFERENCES counties(id),
  CONSTRAINT FK_addresses_country_id FOREIGN KEY (country_id) REFERENCES countries(id)*/
);

--
-- Batch tables
-- Parts are created in batches. This holds the data
--  qty: Quantity of parts made
--  date_start: Date batch was started
--  date_end: date batch ended
--

CREATE TABLE IF NOT EXISTS batches (
  id SERIAL PRIMARY KEY,
  qty INTEGER NOT NULL,
  date_start TIMESTAMP WITH TIME ZONE NOT NULL,
  date_end TIMESTAMP WITH TIME ZONE NOT NULL
);

-- TODO: Find out what `part_batch_id` is there for.
CREATE TABLE IF NOT EXISTS batch_parts_in (
  id SERIAL PRIMARY KEY,
  part_id INTEGER NOT NULL,
  batch_id INTEGER NOT NULL,
  qty INTEGER NOT NULL,
  supplier_order_id INTEGER NULL,
  part_batch_id INTEGER NULL /*,
  CONSTRAINT FK_batch_parts_part_id FOREIGN KEY (part_id) REFERENCES parts(id),
  CONSTRAINT FK_batch_parts_batch_id FOREIGN KEY (batch_id) REFERENCES batches(id)*/
);

CREATE TABLE IF NOT EXISTS batch_parts_out (
  id SERIAL PRIMARY KEY,
  part_id INTEGER NOT NULL,
  batch_id INTEGER NOT NULL,
  qty INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS batch_product_out (
  id SERIAL PRIMARY KEY,
  product_model_id INTEGER NOT NULL,
  product_id INTEGER NOT NULL,
  batch_id INTEGER NOT NULL
);

--
-- List employees who made batch
--

CREATE TABLE IF NOT EXISTS batch_employee_maps (
  id SERIAL PRIMARY KEY,
  batch_id INTEGER NOT NULL,
  employee_id INTEGER NOT NULL /*,
  CONSTRAINT FK_batch_employees_batch_id FOREIGN KEY (batch_id) REFERENCES batches(id),
  CONSTRAINT FK_batch_employees_employees_id FOREIGN KEY (employee_id) REFERENCES employees(id)*/
);

--
-- Table for counties
--
 
CREATE TABLE IF NOT EXISTS counties (
  id SERIAL PRIMARY KEY,
  name VARCHAR(128) NOT NULL
);

--
-- Table for countries
--

CREATE TABLE IF NOT EXISTS countries (
  id SERIAL PRIMARY KEY,
  name VARCHAR(128) NOT NULL,
  code VARCHAR(2) NOT NULL,
  currency_id INTEGER NOT NULL /*,
  CONSTRAINT FK_countries_currency_id FOREIGN KEY (currency_id) REFERENCES currencies(id)*/
);

--
--
--
--

CREATE TABLE IF NOT EXISTS currencies (
  id SERIAL PRIMARY KEY,
  name VARCHAR(128) NOT NULL,
  symbol VARCHAR(5) NOT NULL,
  code VARCHAR(5) NOT NULL, -- Three letter ISO code
  decimal_places INTEGER NOT NULL
);


CREATE TABLE IF NOT EXISTS currency_conversions (
  id SERIAL PRIMARY KEY,
  currency_from_id INTEGER NOT NULL,
  currency_to_id INTEGER NOT NULL,
  rate NUMERIC NOT NULL /*,
  -- TODO: Should these constraints be renamed.
  CONSTRAINT FK_currency_conversions_from_id FOREIGN KEY (currency_from_id) REFERENCES currencies(id),
  CONSTRAINT FK_currency_conversions_to_id FOREIGN KEY (currency_to_id) REFERENCES currencies(id)*/
);

CREATE TABLE IF NOT EXISTS currency_country_maps (
  id SERIAL PRIMARY KEY,
  currency_id INTEGER NOT NULL,
  country_id INTEGER NOT NULL /*,
  CONSTRAINT FK_currency_country_currency_id FOREIGN KEY (currency_id) REFERENCES currencies(id),
  CONSTRAINT FK_currency_country_country_id FOREIGN KEY (country_id) REFERENCES countries(id)*/
);

--
-- Table which lists customers
--

CREATE TABLE IF NOT EXISTS customers (
  id SERIAL PRIMARY KEY,
  title VARCHAR(20) NOT NULL,
  first_name VARCHAR(100) NOT NULL,
  first_name_lower VARCHAR(100) NOT NULL,
  last_name VARCHAR(100) NOT NULL,
  last_name_lower VARCHAR(100) NOT NULL,
  email VARCHAR(100) NULL UNIQUE,
  user_id INTEGER NULL,
  gender CHAR(1) NULL, -- 'm' or 'f'
  date_of_birth DATE NULL /*,
  CONSTRAINT FK_customers_user_id FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE SET NULL*/
);

CREATE TABLE IF NOT EXISTS customer_address_maps (
  id SERIAL PRIMARY KEY,
  customer_id INTEGER NOT NULL,
  address_id INTEGER NOT NULL,
  type VARCHAR(50) NOT NULL /*, -- TODO: Should this link to a table?
  CONSTRAINT FK_customer_address_customer_id FOREIGN KEY (customer_id) REFERENCES customers(id),
  CONSTRAINT FK_currency_country_country_id FOREIGN KEY (country_id) REFERENCES countries(id)*/
);

-- TODO: Delete
/*CREATE TABLE IF NOT EXISTS customer_random (
  id SERIAL PRIMARY KEY,
  random VARCHAR(1024) NOT NULL,
  customer_id INTEGER NULL,
  used BOOLEAN DEFAULT FALSE,
  after BOOLEAN DEFAULT FALSE
);*/

--
-- Discount table
-- name: Name of Discount
-- price: Discount in terms of price
-- percent: Discount in terms of percent
--

CREATE TABLE IF NOT EXISTS discounts (
  id SERIAL PRIMARY KEY,
  name VARCHAR(128) NOT NULL,
  description TEXT NULL,
  fixed NUMERIC NULL,
  fixed_currency_id INTEGER NULL,
  percent NUMERIC NULL /*,
  CONSTRAINT FK_discounts_fixed_currency_id FOREIGN KEY (fixed_currency_id) REFERENCES currencies(id)*/
);

--
-- Lists employees and all data associated to them (names etc)
-- TODO: Fill in, add stuff like staff management
--

CREATE TABLE IF NOT EXISTS employees (
  id SERIAL PRIMARY KEY
);

--
-- Table for images, has their path, dimensions and alt text
--
 
CREATE TABLE IF NOT EXISTS images (
  id SERIAL PRIMARY KEY,
  path VARCHAR(1024) NOT NULL,
  thumbnail_path VARCHAR(1024) NULL,
  alt VARCHAR(512) NULL,
  width INTEGER NULL,
  height INTEGER NULL
);

--
-- The location table, could be used to say where something is stored in the factory
--

CREATE TABLE IF NOT EXISTS locations (
  id SERIAL PRIMARY KEY,
  name VARCHAR(512) NOT NULL
);

--
-- Order tables 
-- Lists all orders
--

CREATE TABLE IF NOT EXISTS orders (
  id SERIAL PRIMARY KEY,
  billing_customer_id INTEGER NOT NULL,
  billing_address_id INTEGER NOT NULL,
  delivery_first_name VARCHAR(100) NOT NULL,
  delivery_last_name VARCHAR(100) NOT NULL,
  delivery_address_id INTEGER NOT NULL,
  price NUMERIC NOT NULL,
  price_currency_id INTEGER NOT NULL,
  completed BOOLEAN NOT NULL DEFAULT FALSE,
  failed BOOLEAN NOT NULL DEFAULT FALSE,
  payment_processor_id INTEGER NOT NULL,
  payment_processor_currency_id INTEGER NULL, 
  payment_detail VARCHAR(1024) NULL,
  shipping_method_id INTEGER NOT NULL,
  date_ordered TIMESTAMP WITH TIME ZONE NOT NULL,
  status_id INTEGER NULL,
  discount_id INTEGER NULL /*,
  CONSTRAINT FK_orders_billing_address_id FOREIGN KEY (address_id) REFERENCES addresses(id),
  CONSTRAINT FK_orders_billing_customer_id FOREIGN KEY (billing_customer_id) REFERENCES customers(id),
  CONSTRAINT FK_orders_delivery_address_id FOREIGN KEY (address_id) REFERENCES addresses(id),
  CONSTRAINT FK_orders_price_currency_id FOREIGN KEY (price_currency_id) REFERENCES currencies(id),
  CONSTRAINT FK_orders_payment_processor_id FOREIGN KEY (payment_processor_id) REFERENCES payment_processors(id),
  CONSTRAINT FK_orders_shipping_method_id FOREIGN KEY (shipping_method_id) REFERENCES shipping(id),
  CONSTRAINT FK_orders_status_id FOREIGN KEY (status_id) REFERENCES statuses(id),
  CONSTRAINT FK_orders_discount_id FOREIGN KEY (discount_id) REFERENCES discounts(id)*/
);

--
-- Links an order to the product models the order includes
--

CREATE TABLE IF NOT EXISTS order_product_model_maps (
  id SERIAL PRIMARY KEY,
  order_id INTEGER NOT NULL,
  model_id INTEGER NOT NULL,
  discount_id INTEGER NULL,
  tax NUMERIC NOT NULL,
  price NUMERIC NOT NULL,
  price_currency_id INTEGER NOT NULL,
  product_qty INTEGER NOT NULL,
  subtotal NUMERIC NOT NULL /*,
  CONSTRAINT FK_order_product_model_order_id FOREIGN KEY (order_id) REFERENCES orders(id),
  CONSTRAINT FK_order_product_model_model_id FOREIGN KEY (model_id) REFERENCES product_models(id),
  CONSTRAINT FK_order_product_model_discount_id FOREIGN KEY (discount_id) REFERENCES discounts(id),
  CONSTRAINT FK_order_product_model_price_currency_id FOREIGN KEY (price_currency_id) REFERENCES currencies(id)*/
);

--
-- Links and order to the individual products in the order
--

CREATE TABLE IF NOT EXISTS order_product_maps (
  id SERIAL PRIMARY KEY,
  order_id INTEGER NOT NULL,
  product_id INTEGER NOT NULL,
  discount_id INTEGER NULL /*,
  CONSTRAINT FK_order_product_order_id FOREIGN KEY (order_id) REFERENCES orders(id),
  CONSTRAINT FK_order_product_product_id FOREIGN KEY (product_id) REFERENCES products(id),
  CONSTRAINT FK_order_product_discount_id FOREIGN KEY (discount_id) REFERENCES discounts(id)*/
);

--
-- Parts Table
-- Lists all parts, including suppliers and stock
-- TODO: If part is made by us say what parts are needed to make it. Completed?
--

CREATE TABLE IF NOT EXISTS parts (
  id SERIAL PRIMARY KEY,
  name VARCHAR(512) NOT NULL,
  description TEXT NULL,
  mass REAL NULL,
  unit VARCHAR(10) NULL,
  price NUMERIC NOT NULL,
  lead_time INTEGER NOT NULL,
  supplier_id INTEGER NULL,
  location_id INTEGER NOT NULL,
  buffer_stock INTEGER NULL,
  stock INTEGER NOT NULL /*,
  CONSTRAINT FK_parts_supplier_id FOREIGN KEY (supplier_id) REFERENCES suppliers(id),
  CONSTRAINT FK_parts_location_id FOREIGN KEY (location_id) REFERENCES locations(id)*/
);

--
-- Creates the relationship between parts and other parts
-- Needs new name
--

CREATE TABLE IF NOT EXISTS part_part_maps (
  id SERIAL PRIMARY KEY,
  parent_part_id INTEGER NOT NULL,
  child_part_id INTEGER NOT NULL, -- This part is used to make the parent one, not other way round
  qty INTEGER NOT NULL /*,
  CONSTRAINT FK_parts_parts_parent_part_id FOREIGN KEY (parent_part_id) REFERENCES parts(id),
  CONSTRAINT FK_parts_parts_child_part_id FOREIGN KEY (child_part_id) REFERENCES parts(id)*/
);

CREATE TABLE IF NOT EXISTS payment_processors (
  id SERIAL PRIMARY KEY,
  name VARCHAR(100) NOT NULL,
  display_name VARCHAR(100) NOT NULL,
  default_processor_currency_id INTEGER NULL /*,
  CONSTRAINT FK_payment_processor_default_processor_currency_id FOREIGN KEY (default_processor_currency_id) REFERENCES payment_processor_currencies(id)*/
);

CREATE TABLE IF NOT EXISTS payment_processor_currencies (
  id SERIAL PRIMARY KEY,
  name VARCHAR(10),
  payment_processor_id INTEGER NOT NULL,
  currency_id INTEGER NOT NULL /*,
  CONSTRAINT FK_payment_processor_currencies_payment_processor_id FOREIGN KEY (payment_processor_id) REFERENCES payment_processors(id),
  CONSTRAINT FK_payment_processor_currencies FOREIGN KEY (currency_id) REFERENCES currencies(id)*/
);

-- Product tables
--

CREATE TABLE IF NOT EXISTS products (
  id SERIAL PRIMARY KEY,
  model_id INTEGER NOT NULL,
  status_id INTEGER NULL,
  location_id INTEGER NULL,
  date_completed TIMESTAMP WITH TIME ZONE NOT NULL /*,
  CONSTRAINT FK_product_model_id FOREIGN KEY (model_id) REFERENCES product_models(id),
  CONSTRAINT FK_product_status_id FOREIGN KEY (status_id) REFERENCES statuses(id),
  CONSTRAINT FK_product_location_id FOREIGN KEY (location_id) REFERENCES locations(id)*/
);

--
-- For WEB
-- Lists the different attributes for a model
-- e.g Colour or Plug
--
 
CREATE TABLE IF NOT EXISTS product_attributes (
  id SERIAL PRIMARY KEY,
  name VARCHAR(256) NOT NULL,
  value VARCHAR(256) NOT NULL
);

--
-- Links a batch and a product together
--

CREATE TABLE IF NOT EXISTS product_batch_maps (
  id SERIAL PRIMARY KEY,
  product_id INTEGER NOT NULL,
  batch_id INTEGER NOT NULL /*,
  CONSTRAINT FK_product_batch_product_id FOREIGN KEY (product_id) REFERENCES products(id),
  CONSTRAINT FK_product_batch_batch_id FOREIGN KEY (batch_id) REFERENCES batches(id)*/
);

--
-- Links a product and an employee together
--

CREATE TABLE IF NOT EXISTS product_employee_maps (
  id SERIAL PRIMARY KEY,
  product_id INTEGER NOT NULL,
  employee_id INTEGER NOT NULL /*,
  CONSTRAINT FK_product_employees_product_id FOREIGN KEY (product_id) REFERENCES products(id),
  CONSTRAINT FK_product_employees_employee_id FOREIGN KEY (employee_id) REFERENCES employees(id)*/
);

--
-- Used for the web. Its the parent of the product model.
-- E.G Single Egg Cooker
--

CREATE TABLE IF NOT EXISTS product_types (
  id SERIAL PRIMARY KEY,
  name VARCHAR(256) NOT NULL,
  description TEXT NULL
);

--
-- Product Model Table
-- Lists the Product model 
--

CREATE TABLE IF NOT EXISTS product_models (
  id SERIAL PRIMARY KEY,
  name VARCHAR(256) NOT NULL,
  description TEXT NULL,
  price NUMERIC NOT NULL,
  price_currency_id INTEGER NOT NULL,
  cost NUMERIC NULL, -- The wholesale cost in case product is brought in
  cost_currency_id INTEGER NULL,
  mass REAL NULL,
  height REAL NULL, -- Used for the shipping method
  width REAL NULL, -- Used for the shipping method
  length REAL NULL,
  type_id INTEGER NOT NULL,
  discount_id INTEGER NULL,
  stock INTEGER NOT NULL /*,
  CONSTRAINT FK_product_models_price_currency_id FOREIGN KEY (price_currency_id) REFERENCES currencies(id),
  CONSTRAINT FK_product_models_cost_currency_id FOREIGN KEY (cost_currency_id) REFERENCES currencies(id),
  CONSTRAINT FK_product_models_type_id FOREIGN KEY (type_id) REFERENCES product_types(id),
  CONSTRAINT FK_product_models_discount_id FOREIGN KEY (discount_id) REFERENCES discounts(id)*/
);

--
-- Links a product model to its attributes that separate it from other models
--

CREATE TABLE IF NOT EXISTS product_model_attribute_maps (
  id SERIAL PRIMARY KEY,
  model_id INTEGER NOT NULL,
  attribute_id INTEGER NOT NULL /*,
  CONSTRAINT FK_product_model_attributes_model_id FOREIGN KEY (model_id) REFERENCES product_models(id),
  CONSTRAINT FK_product_model_attributes_attribute_id FOREIGN KEY (attribute_id) REFERENCES product_attributes(id)*/
);

--
-- The countries that models are available in.
--

CREATE TABLE IF NOT EXISTS product_model_country_maps (
  id SERIAL PRIMARY KEY,
  model_id INTEGER NOT NULL,
  country_id INTEGER NOT NULL
);

--
-- Links a product model with an image
--
CREATE TABLE IF NOT EXISTS product_model_image_maps (
  id SERIAL PRIMARY KEY,
  model_id INTEGER NOT NULL,
  image_id INTEGER NOT NULL,
  image_order INTEGER NULL /* The order in which images are displayed. */ /*,
  CONSTRAINT FK_product_model_image_model_id FOREIGN KEY (model_id) REFERENCES product_model(id),
  CONSTRAINT FK_product_model_image_image_id FOREIGN KEY (image_id) REFERENCES images(id)*/
);

--
-- Links a product model to the parts that are needed to create it
--

CREATE TABLE IF NOT EXISTS product_model_part_maps (
  id SERIAL PRIMARY KEY,
  model_id INTEGER NOT NULL,
  part_id INTEGER NOT NULL,
  part_qty INTEGER NOT NULL /*,
  CONSTRAINT FK_product_model_parts_model_id FOREIGN KEY (model_id) REFERENCES product_model(id),
  CONSTRAINT FK_product_model_parts_part_id FOREIGN KEY (part_id) REFERENCES parts(id)*/
);

CREATE TABLE IF NOT EXISTS product_model_shipping_maps (
  id SERIAL PRIMARY KEY,
  model_id INTEGER NOT NULL,
  shipping_id INTEGER NOT NULL /*, -- The methods with which this model can be shipped
  CONSTRAINT FK_product_model_shipping_model_id FOREIGN KEY (model_id) REFERENCES product_models(id),
  CONSTRAINT FK_product_model_shipping_shipping_id FOREIGN KEY (shipping_id) REFERENCES shipping(id)*/
);

CREATE TABLE IF NOT EXISTS product_model_tax_maps (
  id SERIAL PRIMARY KEY,
  model_id INTEGER NOT NULL,
  tax_id INTEGER NOT NULL /*,
  CONSTRAINT FK_product_model_tax_model_id FOREIGN KEY (model_id) REFERENCES product_models(id),
  CONSTRAINT FK_product_model_tax_tax_id FOREIGN KEY (tax_id) REFERENCES taxes(id)*/
);

-- TODO: Is this needed?
CREATE TABLE IF NOT EXISTS promotions (
  id SERIAL PRIMARY KEY,
  name VARCHAR(100) NOT NULL,
  start_date TIMESTAMP WITH TIME ZONE NOT NULL,
  end_date TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE IF NOT EXISTS promotion_discount_maps (
  id SERIAL PRIMARY KEY,
  promotion_id INTEGER NOT NULL,
  discount_id INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS shipping (
  id SERIAL PRIMARY KEY,
  name VARCHAR(512) NOT NULL,
  price NUMERIC NOT NULL,
  price_currency_id INTEGER NOT NULL,
  description TEXT NULL,
  min_mass REAL NULL,
  max_mass REAL NULL,
  min_width REAL NULL,
  max_width REAL NULL,
  min_length REAL NULL,
  max_length REAL NULL,
  min_height REAL NULL,
  max_height REAL NULL /*,
  CONSTRAINT FK_shipping_price_currency_id FOREIGN KEY (price_currency_id) REFERENCES currencies(id)*/
);

CREATE TABLE IF NOT EXISTS shipping_tax_maps (
  id SERIAL PRIMARY KEY,
  shipping_id INTEGER NOT NULL,
  tax_id INTEGER NOT NULL /*,
  CONSTRAINT FK_shipping_tax_shipping_id FOREIGN KEY (shipping_id) REFERENCES shipping(id),
  CONSTRAINT FK_shipping_tax_tax_id FOREIGN KEY (tax_id) REFERENCES taxes(id)*/
);

CREATE TABLE IF NOT EXISTS shipping_country_maps (
  id SERIAL PRIMARY KEY,
  shipping_id INTEGER NOT NULL,
  country_id INTEGER NOT NULL /*,
  CONSTRAINT FK_shipping_country_shipping_id FOREIGN KEY (shipping_id) REFERENCES shipping(id),
  CONSTRAINT FK_shipping_country_countries_id FOREIGN KEY (country_id) REFERENCES countries(id)*/
);

--
-- Status Table
--

CREATE TABLE IF NOT EXISTS statuses (
  id SERIAL PRIMARY KEY,
  name VARCHAR(256) NOT NULL
);

--
-- The suppliers table
-- Include information about suppliers as well as contact data
--

CREATE TABLE IF NOT EXISTS suppliers (
  id SERIAL PRIMARY KEY,
  name VARCHAR(256) NOT NULL,
  address_id INTEGER NOT NULL,
  contact_name_first VARCHAR(128) NOT NULL,
  contact_name_last VARCHAR(128) NOT NULL,
  contact_number_mobile INTEGER NULL,
  contact_number_telephone INTEGER NULL,
  contact_email VARCHAR(512) NULL,
  contact_email_template TEXT NULL /*,
  CONSTRAINT FK_suppliers_address_id FOREIGN KEY (address_id) REFERENCES addresses(id)*/
);

--
-- Where we order items from the suppliers
--
 
CREATE TABLE IF NOT EXISTS supplier_orders (
  id SERIAL PRIMARY KEY,
  supplier_id INTEGER NOT NULL,
  date_ordered TIMESTAMP WITH TIME ZONE NOT NULL,
  total_price NUMERIC NOT NULL /*,
  CONSTRAINT FK_suppliers_orders_supplier_id FOREIGN KEY (supplier_id) REFERENCES suppliers(id)*/
);

--
-- Links a supplier order with the items being ordered
-- Either parts or products
--

CREATE TABLE IF NOT EXISTS supplier_order_item (
  id SERIAL PRIMARY KEY,
  order_id INTEGER NOT NULL,
  model_id INTEGER NULL,
  part_id INTEGER NULL,
  qty INTEGER NOT NULL /*,
  CONSTRAINT FK_suppliers_orders_items_order_id FOREIGN KEY (order_id) REFERENCES suppliers_orders(id),
  CONSTRAINT FK_suppliers_orders_items_model_id FOREIGN KEY (model_id) REFERENCES product_models(id),
  CONSTRAINT FK_suppliers_orders_items_part_id FOREIGN KEY (part_id) REFERENCES parts(id)*/
);

CREATE TABLE IF NOT EXISTS taxes (
  id SERIAL PRIMARY KEY,
  name VARCHAR(128) NOT NULL,
  percentage NUMERIC NULL,
  fixed NUMERIC NULL,
  fixed_currency_id INTEGER NULL /*,
  CONSTRAINT FK_taxes_fixed_currency_id FOREIGN KEY (fixed_currency_id) REFERENCES currencies(id)*/
);

CREATE TABLE IF NOT EXISTS tax_country_maps (
  id SERIAL PRIMARY KEY,
  tax_id INTEGER NOT NULL,
  country_id INTEGER NOT NULL /*,
  CONSTRAINT FK_tax_country_tax_id FOREIGN KEY (tax_id) REFERENCES taxes(id),
  CONSTRAINT FK_tax_country_country_id FOREIGN KEY (country_id) REFERENCES countries(id)*/
);

CREATE TABLE IF NOT EXISTS towns (
  id SERIAL PRIMARY KEY,
  name VARCHAR(512) NOT NULL
);

CREATE TABLE IF NOT EXISTS users (
  id SERIAL PRIMARY KEY,
  email VARCHAR(100) NOT NULL UNIQUE,
  password VARCHAR(256) NOT NULL, -- Hex representation of password.
  salt VARCHAR(128) NOT NULL,
  date_created TIMESTAMP WITH TIME ZONE NOT NULL,
  active BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE TABLE IF NOT EXISTS users_codes (
  id SERIAL PRIMARY KEY,
  date_start TIMESTAMP WITH TIME ZONE NULL,
  date_end TIMESTAMP WITH TIME ZONE NULL,
  code VARCHAR(128) NOT NULL UNIQUE,
  type VARCHAR(64) NOT NULL,
  used BOOLEAN NOT NULL DEFAULT FALSE,
  user_id INTEGER NOT NULL
);