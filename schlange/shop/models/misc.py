from schlange.data.database import db

class Image(db.Model):
    __tablename__ = 'images'
    id = db.Column(db.Integer, primary_key=True)
    path = db.Column(db.String(1024), nullable=False)
    thumbnail_path = db.Column(db.String(1024))
    alt = db.Column(db.String(512))
    width = db.Column(db.Integer)
    height = db.Column(db.Integer)

# TODO: Payment processor should maybe be in currency.py?
# Look at http://bit.ly/1jVu2fG

class Status(db.Model):
    __tablename__ = 'statuses'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256), nullable=False)
