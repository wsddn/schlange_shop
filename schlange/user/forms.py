from flask_wtf import Form
from wtforms.fields import (BooleanField, PasswordField, StringField)
from wtforms.fields.html5 import EmailField
from wtforms.validators import (EqualTo, InputRequired, Optional, Length,
                                ValidationError)

from schlange.data.database import db
from schlange.user.models.user import User

# TODO: Document these.
class LoginForm(Form):
    email = EmailField('Email Address', [InputRequired()])
    password = PasswordField('Password', [InputRequired()])
    remember = BooleanField('Remember Me', [Optional()])

    def validate(self):
        # TODO: See if this can be a validator(s).
        # TODO: See if the message can be more specific / changed
        if not super().validate():
            return False

        user = User.query.filter(User.email==self.email.data).first()
        error_msg = 'Email address or password is wrong'
        if user is None:
            # User is not found.
            self.email.errors.append(error_msg)
            return False

        if not user.check_password(self.password.data):
            # Password is wrong.
            self.email.errors.append(error_msg)
            return False

        return True
        

def EmailUniqueInDb(form, field):
    # TODO: Should checking the email be here? Maybe not.
    # TODO: Change the message.
    # TODO: Should be made into class?
    # TODO: Use exists() (may be faster)?
    if User.query.filter(User.email == field.data).count() > 0:
        raise ValidationError('This email is already used by another account')

class RegisterForm(Form):
    email = EmailField('Email Address', [InputRequired(), EmailUniqueInDb])
    password = PasswordField('Password', [
        InputRequired(),
        EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('Repeat Password')
    accept_tos = BooleanField('I accept the terms of service',
                              [InputRequired()])

class ActivationForm(Form):
    code = StringField('Activation Code', [InputRequired()])

class RecoveryForm(Form):
    email = EmailField('Email Address', [InputRequired()])