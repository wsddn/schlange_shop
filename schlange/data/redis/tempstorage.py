# TODO: Implement proper exceptions like IndexError

class RedisTempStorage(object):
    def __init__(self, key_prefix, uuid, redis, expiry_time=60*60):
        self.redis = redis
        self.key = '{0}:{1}'.format(key_prefix, uuid)
        self.expiry_time = expiry_time

    def __len__(self):
        return self.redis.hlen(self.key)

    def __getitem__(self, field):
        return self.get(field)

    def __setitem__(self, field, data):
        self.add(field, data)

    def __delitem__(self, field):
        self.delete(field)

    def __contains__(self, field):
        return self.exists(field)

    def __iter__(self):
        return self.keys()

    def update_expiry(self, time=None, pipe=None):
        if pipe is None:
            pipe = self.redis

        if time is None:
            time = self.expiry_time

        pipe.expire(self.key, time)

    def add(self, field, data):
        self.update_expiry()
        self.redis.hset(self.key, field, data)

    def get(self, field):
        self.update_expiry()
        if isinstance(field, list):
            return self.redis.hmget(self.key, field)
        return self.redis.hget(self.key, field)

    def get_all(self):
        self.update_expiry()
        return self.redis.hgetall(self.key)

    def keys(self):
        return self.redis.hkeys(self.key)

    def values(self):
        return self.redis.hvals(self.key)

    def items(self):
        return self.get_all().items()

    def update(self, field, data):
        self.add(field, data)

    def delete(self, field):
        self.update_expiry()
        self.redis.hdel(self.key, field)

    def delete_all(self):
        self.redis.delete(self.key)

    def exists(self, field):
        if isinstance(field, list):
            pipe = self.redis.pipeline()
            self.update_expiry(None, pipe)
            for f in field:
                pipe.hexists(self.key, f)
            result = pipe.execute()
            # True if result list doesn't contain False
            return False not in result

        self.update_expiry()
        return self.redis.hexists(self.key, field)
