"use strict";

var qtyInputs = document.querySelectorAll('.cart-qty');

for (var i = 0; i < qtyInputs.length; i++) {
	qtyInputs[i].addEventListener('change', function(e) {
		var itemElem = e.currentTarget;
		while (itemElem.className !== 'cart-item') {
			itemElem = itemElem.parentElement;
		}

		var postData = new FormData();
		postData.append('product', itemElem.dataset.id);
		postData.append('quantity', e.currentTarget.value);

		var postRequest = new XMLHttpRequest();
		postRequest.open('POST', '/shop/cart/update', false)
		postRequest.send(postData);

		var getRequest = new XMLHttpRequest();
		getRequest.open('GET', '/shop/cart/get', false);
		getRequest.send(null);

		updateCart(getRequest.responseText);
	});
}

function updateCart(jsonResponce) {
	var cartData = JSON.parse(jsonResponce);
	var cartView = document.querySelector('#cart-view');
	var cartItems = cartView.querySelectorAll('.cart-item');

	for (var i = 0; i < cartItems.length; i++) {
		if (cartData.products.hasOwnProperty(cartItems[i].dataset.id)) {
			var productData = cartData.products[cartItems[i].dataset.id];
			cartItems[i].querySelector('.cart-subtotal').innerHTML = 
				productData['subtotal'];
		} else {
			cartItems[i].remove();
		}
	}

	if (cartView.querySelectorAll('.cart-item').length === 0) {
		location.reload(true);
	}
}