from flask import (Blueprint, render_template, request, redirect,
                   url_for)
from flask.ext.login import login_required, current_user

from schlange.shop.models.order import Order

blueprint = Blueprint('invoice', __name__, template_folder='templates',
                      static_folder='static')

@login_required
@blueprint.route('/<int:order_id>')
def invoice(order_id):
	user = current_user
	order = Order.query.filter(
		Order.id == order_id,
		Order.billing_customer.id == user.id
	).first()