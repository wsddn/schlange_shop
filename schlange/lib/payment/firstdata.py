"""
This module handles communication with First Data payment provider.

Proprietary.
"""

from binascii import hexlify
from collections import namedtuple
from hashlib import sha1
from flask import render_template
from schlange.data.database import db
from schlange.lib.constanttime import constant_time_eq
from schlange.shop.models.order import Order
from schlange.shop.models.currency import (
    PaymentProcessor,
    PaymentProcessorCurrency,
    convert_currency
)

# TODO: Do we need a result tuple?
Result = namedtuple('Result', ['success', 'order', 'detail'])

class FirstData(object):
    """FirstData class provides a library of functions to deal with
    sending payment data to FirstData and receiving data from FirstData"""

    # TODO: Allow the URL to be changed for testing.
    #'https://www.ipg-online.com/connect/gateway/processing'
    SUBMIT_URL = 'https://test.ipg-online.com/connect/gateway/processing'
    
    # If we have to redirect to a page by the payment processor. We don't.
    REDIRECT = False 
    REDIRECT_URL = None

    # Data to be included in the outgoing checksum.
    # TODO: Does this have to be a constant. Should it be inside the function.
    CHECKDUM_DATA = ['storename', 'txndatetime', 'chargetotal', 'currency']
    NAME = 'firstdata'

    def __init__(self, app=None):
        # TODO: Doc this.
        self.store_id = None
        self.shared_secret = None

        # TODO: Rename this.
        self.db = PaymentProcessor.query.\
            filter_by(name=self.NAME).first()

        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        # TODO: Document this.
        self.store_id = app.config.get('FIRSTDATA_STORE_ID')
        self.shared_secret = app.config.get('FIRSTDATA_SHARED_SECRET')

    @property
    def id(self):
        # TODO: Document this.
        return self.db.id 

    def render_form(self, order, response_url):
        """Renders the form to be submitted to the FirstData payment gateway,
        using the given order and the url that FirstData will reply to"""
        # TODO: Document this more.
        processor_currency = PaymentProcessorCurrency.query.filter_by(
            payment_processor_id = self.id,
            currency_id = order.price_currency_id
        ).first()

        charge_total = order.price

        if processor_currency is None:
            # Processor does not support order currency natively.
            # Have to convert the charge total into the default currency.
            processor_currency = self.db.default_processor_currency
            charge_total = convert_currency(order.price, order.currency_id,
                                            processor_currency.currency_id)

        # Update the order to show the payment_processor_id.
        # TODO: Do this before? Do this when order is made. Remove NULL.
        order.payment_processor_currency_id = processor_currency.id
        db.session.add(order)
        db.session.commit()

        hidden_attributes = {
            'txntype': 'sale',
            'storename': self.store_id,
            'mode': 'payonly',
            'timezone': 'GMT',
            'txndatetime': order.date_ordered.strftime('%Y:%m:%d-%H:%M:%S'),
            'oid': order.id,
            'chargetotal': '{:.2f}'.format(charge_total),
            'currency': processor_currency.name,
            'responseSuccessURL': response_url,
            'responseFailURL': response_url
        }

        checksum_list = [hidden_attributes[a] for a in self.CHECKDUM_DATA]
        checksum_list.append(self.shared_secret)
        checksum_hex = hexlify(''.join(checksum_list).encode('utf-8'))
        hidden_attributes['hash'] = sha1(checksum_hex).hexdigest()

        return render_template('firstdata/form.html',
                                gateway_url=self.SUBMIT_URL,
                                attributes=iter(hidden_attributes.items()))

    def handle_responce(self, request):
        """Takes a flask.request object and handles FirstData's response.

        Returns:
            namedtuple of Result(success, order, detail). success is boolean
            order is the current order. detail is (if it exists) extra detail
            in case of error
        """
        # FIXME: Is this code ugly?
        # FIXME: Do the try..except loops inhibit code?
        # TODO: Do we need all the try..except
        # TODO: Should exceptions be propagated
        args = request.form

        try:
            order = Order.query.get(args['oid'])
        except KeyError:
            return Result(False, None, 'No order id.')

        if order is None:
            return Result(False, None, 'Malformed First Data response')

        if not self.db.id == order.payment_processor_id:
            return Result(False, None, 'Wrong Payment Processor given.')

        # TODO: Wrap this around in a try..except
        # TODO: Document this.
        processor_currency = PaymentProcessorCurrency.query.get(
            order.payment_processor_currency_id
        )

        charge_total = order.price
        if processor_currency.currency_id != order.price_currency_id:
            # The payment processor doesn't natively support the order
            # currency. Conversion is necessary,
            charge_total = convert_currency(order.price,
                                            order.price_currency_id,
                                            processor_currency.currency_id)

        try:
            checksum_list = [
                self.shared_secret,
                args['approval_code'],
                '{:.2f}'.format(charge_total),
                charge_total,
                order.date_ordered.strftime('%Y:%m:%d-%H:%M:%S'),
                self.store_id
            ]
        except KeyError:
            return Result(False, order, 'Malformed Message')

        checksum_hex = hexlify(''.join(checksum_list).encode('utf-8'))
        checksum = sha1(checksum_hex).hexdigest()

        try:
            their_checksum = args['response_hash']
        except KeyError:
            return Result(False, order, 'Malformed Message')

        # Constant time compare for hash.
        checksum_valid = constant_time_eq(checksum, their_checksum)
        
        if checksum_valid is False:
            return Result(False, order, 'Invalid hash.')

        # TODO: Should we allow success to be None? Thats not boolean :(
        status = args['approval_code'][:1].upper()
        if status == 'Y':
            success = True
        elif status == '?':
            success = None
        elif status == 'N':
            success = False

        try:
            return Result(success, order, args['fail_reason'])
        except KeyError:
            return Result(success, order, None)
