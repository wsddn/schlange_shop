"""
The product module handles the displaying of product types and individual
models.
"""

# TODO: Don't hard-code the country
# TODO: Calculate price properly (conversion)

from flask import (Blueprint, render_template, request)
from json import loads
from schlange.shop.models import (Country, Image, ProductAttribute, ProductModel,
                                  ProductType)
from schlange.shop.models.product import product_model_image_maps
from schlange.lib.localisation import Localisation
from schlange.data.database import db
from schlange.data.database.func import array_agg
from sqlalchemy.sql import func

blueprint = Blueprint('product', __name__, template_folder='templates',
                      static_folder='static')


@blueprint.route('/')
def index():
    """Return a HTML rendered list of available product types."""

    # TODO: Look up country properly.
    country = Country.query.get(1)
    types = ProductType.query.\
        filter(ProductModel.countries.contains(country)).all()

    return render_template('product/index.html', types=types)


@blueprint.route('/view/<int:type_id>')
def view(type_id):
    """Return more detail the given product type. Also allow user to view
    and change product models."""

    country = Country.query.get(1)
    product_type = ProductType.query.\
        join(ProductModel).\
        filter(
            ProductModel.countries.contains(country),
            ProductType.id == int(type_id)
        ).\
        first_or_404()

    # Get a dict of attribute name and then all attributes.
    attributes = db.session.query(
            ProductAttribute.name,
            array_agg(ProductAttribute.value, ProductModel.id.asc())
        ).\
        join((ProductModel, ProductAttribute.products)).\
        filter(ProductModel.type_id == type_id).\
        group_by(ProductAttribute.name).\
        all()

    try:
        # TODO: Is try nicer than if.
        model = product_type.models[0]
    except TypeError:
        # TODO: Give a more informative error page.
        return abort(404)

    return render_template(
        'product/view.html',
        product_attributes=attributes,
        product_model=model,
        images=model.sorted_images,
        product_type=product_type,
        locale=Localisation()
    )


@blueprint.route('/view/_update_product', methods=['POST'])
def update_product():
    """Return the desired product model, based upon the received product
    type id and the JSON encoded dict of attributes the model must have."""

    attributes = loads(request.form['attributes'])
    product_type_id = int(request.form['type_id'])

    # Get the product model that fits all the attributes
    product_model = ProductModel.query.\
        join((ProductAttribute, ProductModel.attributes)).\
        filter(
            ProductModel.type_id == product_type_id,
            ProductAttribute.name.in_(list(attributes.keys())),
            ProductAttribute.value.in_(list(attributes.values()))
        ).\
        group_by(ProductModel.id).\
        having(func.count(ProductAttribute.id) == len(attributes)).\
        first()

    if product_model is None:
        return render_template('product/_no_model.html')

    return render_template(
        'product/_model_view.html',
        product_model=product_model,
        images=product_model.sorted_images
    )
