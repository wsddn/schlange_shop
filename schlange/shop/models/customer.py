from schlange.data.database import db

# TODO: Should this be a class?
# TODO: Reorganise into CustomerAddresses
customer_address_maps = db.Table(
    'customer_address_maps',
    db.Column('customer_id', db.Integer, db.ForeignKey('customers.id'),
              nullable=False),
    db.Column('address_id', db.Integer, db.ForeignKey('addresses.id'),
              nullable=False),
    db.Column('type', db.String(50), nullable=False)
)

# TODO: Find a good way to normalise the data.
# TODO: Look at normalising data in SQL. (LCASE)
class Customer(db.Model):
    __tablename__ = 'customers'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(20), nullable=False)
    first_name = db.Column(db.String(100), nullable=False)
    first_name_lower = db.Column(db.String(100), nullable=False)
    last_name = db.Column(db.String(100), nullable=False)
    last_name_lower = db.Column(db.String(100), nullable=False)
    email = db.Column(db.String(100))
    gender = db.Column(db.String(1))
    date_of_birth = db.Column(db.Date)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    
    addresses = db.relationship('Address',
        secondary=customer_address_maps
    )

    def add_address(self, address_id, address_type):
        count = db.session.query(customer_address_maps).filter_by(
            customer_id = self.id,
            address_id = address_id,
            type = address_type
        ).count()

        if count == 0:
            # Address does not already exist. Add it.
            db.session.execute(customer_address_maps.insert().values(
                customer_id = self.id,
                address_id = address_id,
                type = address_type
            ))


MALE_TITLES = ['mr', 'master']
FEMALE_TITLES = ['miss', 'mrs', 'ms']

def gender_from_title(title):
    if title.lower() in MALE_TITLES:
        return 'm'
    elif title.lower() in FEMALE_TITLES:
        return 'f'

def get_customer(first_name, last_name, email):
    first_lower = first_name.lower()
    last_lower = last_name.lower()
    email = email.lower()

    return Customer.query.filter_by(first_name_lower=first_lower,
                                    last_name_lower=last_lower,
                                    email=email).first()
