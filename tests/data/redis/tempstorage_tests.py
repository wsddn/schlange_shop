from time import sleep
import unittest

from redis import Redis

import schlange.data.redis.tempstorage as storage

class RedisTempStorageTest(unittest.TestCase):
    def setUp(self):
        self.store = storage.RedisTempStorage('temp_test', '12345', Redis())

    def tearDown(self):
        self.store.delete_all()

    def test__len__(self):
        for i in range(100):
            self.store.add('test'+str(i), i)
        assert len(self.store) == 100

    def test_get(self):
        self.store.redis.hset(self.store.key, 'get_test', b'23')
        self.store.redis.hset(self.store.key, 'another', b'Yeezus')

        assert self.store.get('another') == b'Yeezus'
        assert self.store.get(['get_test', 'another']) == [b'23', b'Yeezus']
        assert self.store['another'] == b'Yeezus'
        assert self.store[['get_test', 'another']] == [b'23', b'Yeezus']

    def test_get_all(self):
        test_dict = {
            b'get_test': b'23',
            b'another': b'Yeezus',
            b'tsget': b'hkjh'
        }
        for field, data in test_dict.items():
             self.store.redis.hset(self.store.key, field, data)

        print(self.store.get_all())
        assert self.store.get_all() == test_dict

    def test_add(self):
        self.store.add('testr', b'pac2')
        assert self.store.redis.hget(self.store.key, 'testr') == b'pac2'

    def test_exists(self):
        self.store.add('kashmir', b'stairway')
        self.store.add('breakdown', b'communication')
        self.store.add('california', b'going')

        assert self.store.exists('kashmir')
        assert not self.store.exists('heaven')
        assert self.store.exists(['breakdown', 'california'])
        assert not self.store.exists(['kashmir', 'gallows'])
        assert not self.store.exists(['levee', 'breaks'])

        assert 'kashmir' in self.store
        assert 'heaven' not in self.store
        assert ['breakdown', 'california'] in self.store

    def test_delete(self):
        self.store.add('alabama', b'home')
        self.store.add('bird', b'free')

        self.store.delete('alabama')
        assert not self.store.exists('alabama')

        del self.store['bird']
        assert not self.store.exists('bird')

    def test_update_expiry(self):
        self.store.add('steps', b'three')
        self.store.update_expiry(1)
        sleep(1.1)
        assert not self.store.exists('steps')