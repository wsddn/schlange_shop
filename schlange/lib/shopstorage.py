from flask import session
from redis import Redis
from simplejson import dumps, loads
from uuid import uuid4

from schlange.data.redis.tempstorage import RedisTempStorage

class ShopStorage(RedisTempStorage):
    UUID_INDEX = 'uuid'
    PREFIX = 'shop'
    def __init__(self, uuid=None, redis=None):
        """Init Cart with redis connection, if None then new connection."""
        if redis is None:
            redis = Redis()

        if uuid is None:
            if self.UUID_INDEX not in session:
                session[self.UUID_INDEX] = str(uuid4())
            uuid = session[self.UUID_INDEX]

        super(ShopStorage, self).__init__(self.PREFIX, uuid, redis)

class CartStorage(ShopStorage):
    """Temporarily store shopping cart data.

    The cart data is stored in redis hash maps, and automatically
    expires after a certain time limit."""

    PREFIX = 'cart'

    def add(self, product_id, quantity):
        """Add a product with given id and quantity to the cart.
        Increments the quantity if the product already exists."""
        self.redis.hincrby(self.key, int(product_id), int(quantity))
        self.update_expiry()

    def get(self, product_id):
        """Return the quantity as an integer for given product id."""
        return int(super(CartStorage, self).get(product_id))

    def get_all(self):
        """Return a dict representation of all items in cart.

        Format:
            {product_id: quantity}"""
        all_data = super(CartStorage, self).get_all()
        return {int(m_id): int(qty) for m_id, qty in all_data.items()}

    def update(self, product_id, quantity):
        """Update a product's quantity.
        If quantity is negative then remove product."""
        quantity = int(quantity)
        if quantity <= 0:
            self.delete(product_id)
        else:
            super(CartStorage, self).add(product_id, quantity)


class CheckoutStorage(ShopStorage):
    """Temporarily store checkout data in redis.

    Checkout data is expected to be taken in as a string. It will
    be JSON encoded and stored in redis.

    Attributes:
        uuid_index: The index in flask.session data that the user's
            uuid is stored.
        expiry_time: The time in seconds that checkout data will expire.
    """

    PREFIX = 'checkout'

    def add(self, stage, data):
        """Add data for a given stage of the checkout.
        Data must be JSON encodeable."""
        stage = stage.encode('utf-8')
        json_data = dumps(data).encode('utf-8')
        super(CheckoutStorage, self).add(stage, json_data)

    def get(self, stage):
        """Return data for given stage."""
        return loads(super(CheckoutStorage, self).get(stage))

    def get_all(self):
        """Return all data in a dict of {'stage': data}"""
        all_data = super(CheckoutStorage, self).get_all()
        return {key.decode('utf-8'): loads(val.decode('utf-8'))
                for key, val in all_data.items()}

